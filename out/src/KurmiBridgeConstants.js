"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.kurmiappconstants = void 0;
const vscode = require("vscode");
exports.kurmiappconstants = {
    kurmi: "Kurmi",
    appname: "kurmiwe",
    appDisplayName: "Kurmi Workspace Editor",
    keyFileSystemReady: "fsready",
    keyAppReady: "ready",
    keyKurmiWorkspaceState: "kurmiWorkspaceState",
    keyKurmiLoginData: "kurmiLoginData",
    keyKurmiSessionLocalFSMapping: "kurmiSessionLocalFSMapping",
    keyFileSystemSchemeMode: "fsscheme",
    virtualFileSystemSchemeName: "kurmifs",
    keyKurmiStoredLoginDatas: "LoginDatas",
    keyFileStateData: "fileStateData",
    keyTargetLocalWorkspaceRootDirectory: "targetLocalWorkspaceRootDirectory",
    preferredProgressLocation: vscode.ProgressLocation.Notification,
    keyPreferredProgressLocation: "preferredProgressLocation",
    keyNextTick: "kurmiNextTick",
    popupTimeout: 20000
};
exports.kurmiappconstants.popupTimeout = vscode.workspace.getConfiguration(exports.kurmiappconstants.appname).get('popupTimeout') || 20000;
let preferredProgressLocation = vscode.workspace.getConfiguration(exports.kurmiappconstants.appname).get(exports.kurmiappconstants.keyPreferredProgressLocation);
console.log("preferredProgressLocation", preferredProgressLocation);
exports.kurmiappconstants.preferredProgressLocation = preferredProgressLocation == "Window" ? vscode.ProgressLocation.Window : vscode.ProgressLocation.Notification;
console.log('Constants', exports.kurmiappconstants);
//# sourceMappingURL=KurmiBridgeConstants.js.map