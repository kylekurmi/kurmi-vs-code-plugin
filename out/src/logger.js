"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.warningMessage = exports.infoMessage = exports.errorMessage = void 0;
const vscode = require("vscode");
const KurmiBridgeConstants_1 = require("./KurmiBridgeConstants");
const outputLogger = vscode.window.createOutputChannel("kurmi-bridge", { log: true });
outputLogger.show();
function errorMessage(title, e = '') {
    let message = e ? "\n" + (typeof e == "string" ? e : JSON.stringify(e)) : "";
    outputLogger.error("[" + title + "] " + message);
    console.error("[" + title + "] " + message);
    vscode.window.showErrorMessage([title].concat(message).join(' - '));
}
exports.errorMessage = errorMessage;
function infoMessage(title, e = '') {
    let message = e ? "\n" + (typeof e == "string" ? e : JSON.stringify(e)) : "";
    vscode.window.withProgress({
        location: KurmiBridgeConstants_1.kurmiappconstants.preferredProgressLocation,
        title: KurmiBridgeConstants_1.kurmiappconstants.appDisplayName,
        cancellable: false,
    }, async (progress, token) => {
        for (let i = 0; i < 10; i++) {
            setTimeout(() => {
                progress.report({ increment: i * 10, message: [title].concat(message).join(' - ') });
            }, KurmiBridgeConstants_1.kurmiappconstants.popupTimeout || 10000);
        }
    });
    outputLogger.info("[" + title + "] " + JSON.stringify(message));
    console.log("[" + title + "] " + JSON.stringify(message));
}
exports.infoMessage = infoMessage;
function warningMessage(title, message = '') {
    outputLogger.warn("[" + title + "] " + JSON.stringify(message));
    console.warn("[" + title + "] " + JSON.stringify(message));
}
exports.warningMessage = warningMessage;
//# sourceMappingURL=logger.js.map