"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KurmiWorkspaceEditor = exports.KurmiFileSystemEmulation = void 0;
const vscode = require("vscode");
const fs = require("fs");
const logger_1 = require("./logger");
const KurmiBridgeConstants_1 = require("./KurmiBridgeConstants");
const path = require("path");
const forwardslash = "/";
const prism = require('kurmi-prism/prism_onnode.js');
var KurmiFileSystemEmulation;
(function (KurmiFileSystemEmulation) {
    KurmiFileSystemEmulation["likeConfigFile"] = "Like configfile.zip";
    KurmiFileSystemEmulation["likeDesignMenu"] = "Like design menu";
    KurmiFileSystemEmulation["flat"] = "flat structure";
})(KurmiFileSystemEmulation || (exports.KurmiFileSystemEmulation = KurmiFileSystemEmulation = {}));
var KurmiWorkspaceEditor;
(function (KurmiWorkspaceEditor) {
    let KurmiEditorMode;
    (function (KurmiEditorMode) {
        KurmiEditorMode[KurmiEditorMode["silent"] = 0] = "silent";
        KurmiEditorMode[KurmiEditorMode["verbose"] = 1] = "verbose";
        KurmiEditorMode[KurmiEditorMode["clone"] = 2] = "clone";
        KurmiEditorMode[KurmiEditorMode["pull"] = 3] = "pull";
    })(KurmiEditorMode = KurmiWorkspaceEditor.KurmiEditorMode || (KurmiWorkspaceEditor.KurmiEditorMode = {}));
    class VSCodeExtensionDataManager {
        constructor(context, prism) {
            this.connections = [];
            this.mode = KurmiEditorMode.silent;
            this.emulationType = KurmiFileSystemEmulation.likeConfigFile;
            this.context = context;
            this.prism = prism;
            //this.progress = vws.progress
            //this.token = vws.token;
            //this.mode = vws.mode;
            this.sessionLocalFSMapping = this.context.workspaceState.get(KurmiBridgeConstants_1.kurmiappconstants.keyKurmiSessionLocalFSMapping) || {};
        }
        getKurmiFileSimulatedUri(workspace, kurmiFilePath) {
            return vscode.Uri.parse(`${KurmiBridgeConstants_1.kurmiappconstants.appname}://${this.currentSession?.connection.host.authority}/${workspace}${kurmiFilePath}`);
        }
        getUri(paths) {
            if (this.currentSession && this.currentSession.connection.baseuri) {
                return vscode.Uri.joinPath(this.currentSession.connection.baseuri, ...paths);
            }
            else {
                throw "No current session!";
            }
        }
        createFolders(workspace, folderList) {
            return Promise.all(folderList.map(folder => this.getUri([workspace, folder])).map(vscode.workspace.fs.createDirectory));
        }
        fetch(workspace) {
            return prism.workspace.listWorkspaceFiles(workspace);
        }
        pull(workspace) {
            let $this = this;
            let files = this.currentSession?.fileQueue || [];
            let folders = this.getFoldersList(files);
            this.createFolders(workspace, folders);
            return vscode.window.withProgress({ title: `Getting from ${workspace}`, location: KurmiBridgeConstants_1.kurmiappconstants.preferredProgressLocation, cancellable: true }, async (progress, token) => {
                await Promise.all(files.map(function (filePath, i, arr) {
                    if (token.isCancellationRequested === true) {
                        return;
                    }
                    else {
                        return $this.pullFile(workspace, filePath).then((createFileResult) => {
                            progress.report({ message: `${filePath}`, increment: 100 * (1 / arr.length) });
                            return createFileResult;
                        });
                    }
                }));
            });
        }
        pullFile(workspace, kurmiFilePath) {
            let $this = this;
            let filePath = kurmiFilePath.replace(/^[\\\/]+/, "");
            let storageUri = $this.getUri([workspace, filePath]);
            let storagePath = storageUri.path;
            var storageData = {};
            //$this.fileStateData.set(storageUri,storageData);
            return this.prism.workspace.getWorkspaceFile(workspace, kurmiFilePath).then(async function (fileResult) {
                if (fileResult) {
                    let exists = fs.existsSync(storageUri.fsPath);
                    var localDoc = exists && await vscode.workspace.openTextDocument(storageUri);
                    if (exists && localDoc && localDoc.getText() != fileResult.content) {
                        var serverDoc = await vscode.workspace.openTextDocument({ content: fileResult.content });
                        vscode.commands.executeCommand("vscode.diff", storageUri, serverDoc.uri);
                    }
                    else {
                        vscode.workspace.fs.writeFile(storageUri, Buffer.from(fileResult.content || ''));
                    }
                    delete fileResult.content;
                    let cs = $this.currentSession;
                    //$this.currentSession?.managedFileData.set(storageUri,fileResult);
                    var data = { ...fileResult };
                    if (data.content) {
                        delete data.content;
                    }
                    if (cs) {
                        data.originHostName = cs && cs.connection.baseuri?.authority;
                        data.workspace = cs && cs.workspaceName;
                    }
                    else {
                        throw "NO CS... COMMIT";
                    }
                    cs?.managedFileData.set(storageUri, data);
                }
                else {
                    throw `File content not found for ${$this.getKurmiFileSimulatedUri(workspace, kurmiFilePath)}`;
                }
                console.log(`Fetched ${$this.getKurmiFileSimulatedUri(workspace, kurmiFilePath)}`);
            }).catch(function (e) {
                vscode.workspace.fs.writeFile(storageUri, Buffer.from(JSON.stringify(e)));
                (0, logger_1.errorMessage)('createVirtualFile', e);
            });
        }
        renameFile(oldUri, newUri) {
            let $this = this;
            var data = this.currentSession?.managedFileData.get(oldUri);
            this.currentSession?.managedFileData.set(newUri, data);
            this.currentSession?.managedFileData.set(oldUri, undefined);
            (0, logger_1.infoMessage)("renameFile", "Updating the fileStateData and saving as a new file");
            var doc = vscode.workspace.textDocuments.find(f => f.uri == newUri);
            if (doc != undefined) {
                this.commitFile(doc).then((res) => {
                    (0, logger_1.infoMessage)("renameFile", "Deleting old kurmi file now that the new one is saved.");
                    this.deleteFiles([oldUri]);
                }).catch((e) => {
                    (0, logger_1.errorMessage)("renameFile", `Didnt save the new file to allow the deletion of the other;`);
                });
            }
            else {
                (0, logger_1.errorMessage)("renameFile", "Didnt find the file in vscode workspace.");
            }
        }
        deleteFiles(files) {
            let $this = this;
            let fileDatas = files.map(f => this.currentSession?.managedFileData.get(f));
            var kurmiPaths = fileDatas.map(({ path }) => path);
            return this.prism.api.execute('deleteWorkspaceFiles', { workspace: this.currentSession?.workspaceName, path: kurmiPaths }).then(function (res) {
                var mo = {};
                mo.modal = true;
                mo.detail = `File Deleted at \r\n\t${fileDatas.map(fd => $this.getKurmiFileSimulatedUri(fd.workspace, fd.path)).join('\t\r\n')}\r\n (${JSON.stringify(res)})`;
                vscode.window.showInformationMessage("Success", mo);
            }, function (e) {
                (0, logger_1.errorMessage)('deleteFiles', e);
            });
        }
        async commitFile(e) {
            let cs = this.currentSession;
            if (!cs) {
                (0, logger_1.errorMessage)("saveFile", "No Current Session");
                throw new Error("saveFile() No Current Session");
            }
            var data = this.currentSession?.managedFileData.get(e.uri);
            if (!data.path) {
                await vscode.window.showInputBox({ prompt: `Enter the path on ws ${cs.workspaceName}`, placeHolder: e.uri.path, title: "Save new file on workspace" })
                    .then((newPath) => {
                    data.path = newPath || '';
                });
            }
            else {
                var serverVersion = await this.prism.workspace.getWorkspaceFile(cs.workspaceName, data.path);
                //vscode.commands.executeCommand('vscode.diff',{left:JSON.parse(serverVersion.content),right:e.getText(),title:"Server Version | Local Version"});
                var serverIsNewer = serverVersion.revisionDate !== data.revisionDate;
                if (serverIsNewer) {
                    var serverDoc = await vscode.workspace.openTextDocument({ content: serverVersion.content });
                    vscode.commands.executeCommand("vscode.diff", e.uri, serverDoc.uri);
                    var qpi = ["Overwrite", "Discard"].map((item) => { return { title: item, label: item }; });
                    var answer = await vscode.window.showErrorMessage("Concurrent update!", { modal: true }, ...qpi);
                    if (answer?.title == "Overwrite") {
                        (0, logger_1.infoMessage)("Overwrote the file!", e.uri.path);
                    }
                    else {
                        (0, logger_1.errorMessage)("Concurrent update!", e.uri.path);
                    }
                }
            }
            if (!data.path) {
                throw "No path set for uploading to workspace!";
            }
            let $this = this;
            let payload = { workspace: cs.workspaceName, path: data.path, content: e.getText(), revisionAuthor: vscode.env.appName, revisionComment: "Updated from vscode extension" };
            let printablePayload = { ...payload };
            printablePayload.content = printablePayload.content.substring(0, 27) + "...";
            printablePayload.origin = cs.connection.baseuri?.authority;
            (0, logger_1.infoMessage)('saveFile', printablePayload);
            var result = await $this.prism.api.execute('addWorkspaceFile', payload).then(async function (res) {
                (0, logger_1.infoMessage)("SaveFile", `File saved in workspace.\r\n${$this.getKurmiFileSimulatedUri(payload.workspace, payload.path)}   `);
                var newServerVersion = await $this.prism.workspace.getWorkspaceFile(payload.workspace, payload.path);
                var data = { ...newServerVersion };
                if (data.content) {
                    delete data.content;
                }
                if (cs) {
                    newServerVersion.originHostName = cs && cs.connection.baseuri?.authority;
                    newServerVersion.workspace = cs && cs.workspaceName;
                }
                else {
                    throw "NO CS... COMMIT";
                }
                cs?.managedFileData.set(e.uri, data);
            }, function (e) {
                (0, logger_1.errorMessage)('Save file', e);
                throw e;
            });
            return result;
        }
        async findConnection(u) {
            let c = this.sessionLocalFSMapping[u.toString()];
            if (c) {
                return new Connection(c.host, c.login, c.password, c.doSave, c.knickName);
            }
            else {
                let splits = (u.path).split(/[\\\/]+/);
                let ksld = JSON.parse(await this.context.secrets.get(KurmiBridgeConstants_1.kurmiappconstants.keyKurmiStoredLoginDatas) || "[]");
                let cc = ksld.find((m) => { return m && m.host && m.hostName && splits.some((s) => new RegExp(m.hostName, "i").test(s)); });
                if (cc) {
                    return new Connection(cc.host, cc.login, cc.password, cc.doSave, cc.knickName);
                }
            }
        }
        async saveFSConnectionMapping() {
            this.setBaseURI();
            let c = this.currentSession?.connection;
            if (c && c.baseuri) {
                this.sessionLocalFSMapping[c?.baseuri?.toString()] = c;
                await this.context.workspaceState.update(KurmiBridgeConstants_1.kurmiappconstants.keyKurmiSessionLocalFSMapping, this.sessionLocalFSMapping);
            }
        }
        async initializeSession(sd) {
            sd.managedFileData = new FileStateDataBroker(this.context);
            sd.pathSeparator = sd.virtual ? "/" : path.sep;
            this.currentSession = sd;
            //proxy function for prism.api.connect();
            if (this.currentSession.connection.connected !== true && typeof this.currentSession.connection.connect == "function") {
                await this.currentSession.connection.connect(prism);
                this.saveFSConnectionMapping();
                this.saveConnections();
                let currentFolder = vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0];
                if (this.currentSession && this.currentSession.connection.baseuri) {
                    if (!currentFolder || currentFolder.uri != this.currentSession.connection.baseuri) {
                        await vscode.workspace.fs.createDirectory(this.currentSession.connection?.baseuri);
                        let lenFolders = vscode.workspace.workspaceFolders?.length || 0;
                        //vscode.workspace.updateWorkspaceFolders(lenFolders,0,{uri:this.currentSession.connection?.baseuri, name: sd.connection?.host.authority});
                        if (lenFolders > 0) {
                            //vscode.workspace.updateWorkspaceFolders(0,lenFolders);
                        }
                    }
                }
            }
        }
        setBaseURI() {
            let c = this.currentSession?.connection;
            if (c && this.currentSession?.virtual) {
                let base = vscode.Uri.parse(`${KurmiBridgeConstants_1.kurmiappconstants.virtualFileSystemSchemeName}://${c.host.authority}/`);
                c.baseuri = base;
            }
            else if (c) {
                let keyTargetLocalWorkspaceRootDirectory = vscode.workspace.getConfiguration(KurmiBridgeConstants_1.kurmiappconstants.appname).get(KurmiBridgeConstants_1.kurmiappconstants.keyTargetLocalWorkspaceRootDirectory) || '';
                let base = vscode.Uri.file(keyTargetLocalWorkspaceRootDirectory);
                c.baseuri = vscode.Uri.joinPath(base, ...[c.host.authority]);
            }
            return c?.baseuri;
        }
        saveConnections() {
            let ksld = this.context.secrets.get(KurmiBridgeConstants_1.kurmiappconstants.keyKurmiStoredLoginDatas) || {};
            this.connections.filter(c => c.doSave).forEach((c) => {
                ksld[c.host.toString()] = c;
            });
        }
        getFoldersList(wsFilesFiltered) {
            var $this = this;
            if (this.emulationType == KurmiFileSystemEmulation.flat) {
                return [];
            }
            if (this.emulationType == KurmiFileSystemEmulation.likeDesignMenu) {
                var list = [];
                wsFilesFiltered.map((w) => this.getKurmiDesignMenuStructureFromFilePath(w)).forEach(function (item) {
                    item.split($this.currentSession?.pathSeparator || path.sep).forEach(function (splt, i, arr) {
                        var propName = arr.slice(0, i + 1).join($this.currentSession?.pathSeparator || path.sep);
                        list.push(propName);
                    });
                });
                return list.filter((item, i, a) => a.indexOf(item) == i);
            }
            var $this = this;
            var folders = {};
            wsFilesFiltered.forEach(function (filePath) {
                filePath.split(forwardslash).slice(0, -1).forEach(function (splt, i, arr) {
                    var propName = arr.slice(1, i + 1).join($this.currentSession?.pathSeparator || path.sep);
                    folders[propName] = true;
                });
            });
            //build a list of directories by splitting the kurmi file paths and joining them at each iteration
            var folderList = Object.keys(folders).map(f => f.replace(/^[\\\/]+/, "").replace(/[\\\/]+$/, "")).filter(f => f != forwardslash && f != "").sort();
            return folderList;
        }
        getFileExtensionFromKurmiFilePath(filePath) {
            let [fileExtension] = filePath.match(/[\w\-\_]+\.\w+$/g) || [''];
            return fileExtension;
        }
        getKurmiDesignMenuStructureFromFilePath(filePath) {
            //get the complementing design menu folder name from the kurmi file path
            var ps = this.currentSession?.pathSeparator;
            let fileExtension = this.getFileExtensionFromKurmiFilePath(filePath);
            if (fileExtension.length == 0) {
                return "";
            }
            switch (fileExtension.toLowerCase()) {
                case "eventlogger.js": {
                    return "Event logger";
                }
                case "apiutil.js": { }
                case "util.js": {
                    return "Javascript libraries";
                }
                case "serviceorder.json": { }
                case "service.xml": {
                    return `Services${ps}Service Factory`;
                }
                case "inference.js": {
                    return `Services${ps}Inference`;
                }
                case "quickfeature.properties": { }
                case "quickfeature.js": { }
                case "quickfeature.xml": {
                    return "Quick features";
                }
                case "advancedconnector.xml": { }
                case "connector.properties": { }
                case "connector.xml": {
                    return "Connectors definitions";
                }
                case "core-entities.xml": {
                    return "User definitions";
                }
                case "directorymodel.xml": {
                    return "Directory connectors";
                }
                case "mail.js": {
                    return "Emails";
                }
                case "rules.json": { }
                case "rulesset.json": { }
                case "ruleset.json": { }
                case "rule.json": {
                    return "Engineering Rules";
                }
                case "import.js": { }
                case "export.js": { }
                case "componentadjustment.js": { }
                case "discovery.js": { }
                case "dashboardscenario.js": { }
                case "dashboardtenant.js": { }
                case "postprocessing.js": { }
                case "externaltask.js": { }
                case "kurmiapi.js": { }
                case "configuration.js": { }
                case "processing.js": { }
                case "data.json": { }
                case "choice.json": { }
                case "scenario.json": { }
                case "mergeadjust.js": { }
                case "schema.json": { }
                case "options.json": {
                    return "Scenarios and schema";
                }
                case "widget.js": {
                    return "Homepage widgets";
                }
                default: {
                    (0, logger_1.warningMessage)("Unsupported file extension: ", fileExtension);
                    return "";
                }
            }
        }
        getKurmiDesignMenuStructurePathFromFilePath(filePath) {
            //returns the design menu structure with the formatted real kurmi path as it would appear in the design menu items page;
            var fileExtension = this.getFileExtensionFromKurmiFilePath(filePath);
            var designPath = this.getKurmiDesignMenuStructureFromFilePath(filePath);
            var suffix = (fileExtension ? filePath.replace(/\//g, "_") : "");
            return [designPath].concat(suffix).join(this.currentSession?.pathSeparator);
        }
    }
    KurmiWorkspaceEditor.VSCodeExtensionDataManager = VSCodeExtensionDataManager;
    class Connection {
        constructor(host, login, password, doSave, knickName) {
            this.connected = false;
            this.doSave = false;
            this.host = host;
            this.login = login;
            this.password = password;
            this.doSave = doSave;
            this.knickName = knickName;
        }
        async connect(prism) {
            this.prism = prism;
            this.prism.api.login(this.login, this.password, `${this.host.scheme}://${this.host.authority}`);
            this.connected = this.prism.api.isConnected;
            let c = await this.prism.api.connect();
            this.lastConnection = new Date();
            return c;
        }
    }
    KurmiWorkspaceEditor.Connection = Connection;
    let KurmiFileSystemEmulation;
    (function (KurmiFileSystemEmulation) {
        KurmiFileSystemEmulation["likeConfigFile"] = "Like configfile.zip";
        KurmiFileSystemEmulation["likeDesignMenu"] = "Like design menu";
        KurmiFileSystemEmulation["flat"] = "flat structure";
    })(KurmiFileSystemEmulation = KurmiWorkspaceEditor.KurmiFileSystemEmulation || (KurmiWorkspaceEditor.KurmiFileSystemEmulation = {}));
    class FileStateDataBroker {
        constructor(context) {
            this.workspaceState = context.workspaceState;
        }
        get(uri) {
            if (!uri)
                return;
            var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
            var data = (this.workspaceState.get(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData) || {});
            return data[key];
        }
        set(uri, newFileData) {
            if (!uri)
                return;
            var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
            var data = (this.workspaceState.get(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData) || {});
            data[key] = newFileData;
            this.workspaceState.update(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData, data);
        }
        Hostget(hostName, workspace, uri) {
            var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
            var data = (this.workspaceState.get(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData) || {});
            var hostData = data && data[hostName];
            var hostWorkspaceData = hostData && hostData[workspace];
            var hostWorkspaceKey = hostWorkspaceData && hostWorkspaceData[key];
            return hostWorkspaceKey || {};
        }
        Hostset(hostName, workspace, uri, newFileData) {
            var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
            var data = (this.workspaceState.get(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData) || {});
            data[hostName] = data[hostName] || {};
            data[hostName][workspace] = data[hostName][workspace] || {};
            data[hostName][workspace][key] = data[hostName][workspace][key] || {};
            data[hostName][workspace][key] = newFileData;
            this.workspaceState.update(KurmiBridgeConstants_1.kurmiappconstants.keyFileStateData, data);
        }
    }
})(KurmiWorkspaceEditor || (exports.KurmiWorkspaceEditor = KurmiWorkspaceEditor = {}));
//# sourceMappingURL=kurmiVirtualWorkspace.js.map