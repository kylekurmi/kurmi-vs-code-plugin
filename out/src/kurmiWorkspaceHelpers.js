"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.chooseSpecificKurmiFilePaths = exports.getWorkspaceFilePaths = exports.filterWorkspaceFiles = exports.getFileTypeFilter = exports.getWorkspaceChoices = exports.getKurmiDesignMenuStructureFromFilePath = exports.whenClauseContext = void 0;
const vscode = require("vscode");
const logger_1 = require("./logger");
const prism = require('kurmi-prism/prism_onnode.js');
exports.whenClauseContext = {
    setReady: function (value, source) {
        console.log("setting ready context to " + value, source);
        vscode.commands.executeCommand('setContext', 'kurmi-pro.ready', value);
    },
    setFSReady: function (value, source) {
        console.log("setting fsready context to " + value, source);
        vscode.commands.executeCommand('setContext', 'kurmi-pro.fsready', value);
    },
    setFSMode: function (context, mode) {
        return context.workspaceState.update('kurmi-pro.fsscheme', mode);
    },
    getReady: async function () {
        return await vscode.commands.executeCommand('getContext', 'kurmi-pro.ready');
    },
    getFSReady: async function () {
        return await vscode.commands.executeCommand('getContext', 'kurmi-pro.fsready');
    },
    getFSMode: function (context) {
        return context.workspaceState.get('kurmi-pro.fsscheme');
    }
};
const pathSeparator = "/";
function getKurmiDesignMenuStructureFromFilePath(filePath) {
    var ps = pathSeparator;
    var [fileExtension] = filePath.match(/[\w\-\_]+\.\w+$/g) || [''];
    if (fileExtension.length == 0) {
        return "";
    }
    switch (fileExtension.toLowerCase()) {
        case "eventlogger.js": {
            return "Event logger";
        }
        case "apiutil.js": { }
        case "util.js": {
            return "Javascript libraries";
        }
        case "serviceorder.json": { }
        case "service.xml": {
            return `Services${ps}Service Factory`;
        }
        case "inference.js": {
            return `Services${ps}Inference`;
        }
        case "quickfeature.properties": { }
        case "quickfeature.js": { }
        case "quickfeature.xml": {
            return "Quick features";
        }
        case "advancedconnector.xml": { }
        case "connector.properties": { }
        case "connector.xml": {
            return "Connectors definitions";
        }
        case "core-entities.xml": {
            return "User definitions";
        }
        case "directorymodel.xml": {
            return "Directory connectors";
        }
        case "mail.js": {
            return "Emails";
        }
        case "rules.json": { }
        case "rulesset.json": { }
        case "ruleset.json": { }
        case "rule.json": {
            return "Engineering Rules";
        }
        case "import.js": { }
        case "export.js": { }
        case "componentadjustment.js": { }
        case "discovery.js": { }
        case "dashboardscenario.js": { }
        case "dashboardtenant.js": { }
        case "postprocessing.js": { }
        case "externaltask.js": { }
        case "kurmiapi.js": { }
        case "configuration.js": { }
        case "processing.js": { }
        case "data.json": { }
        case "choice.json": { }
        case "scenario.json": { }
        case "mergeadjust.js": { }
        case "schema.json": { }
        case "options.json": {
            return "Scenarios and schema";
        }
        case "widget.js": {
            return "Homepage widgets";
        }
        default: {
            (0, logger_1.warningMessage)("Unsupported file extension: ", fileExtension);
            return "";
        }
    }
}
exports.getKurmiDesignMenuStructureFromFilePath = getKurmiDesignMenuStructureFromFilePath;
;
function getWorkspaceChoices(suggestedWorkspace) {
    if (!prism.api.isConnected) {
        throw ["Kurmi is not connected", "Please reconnect."];
    }
    const forkedIcon = "└ ";
    var masterList = [];
    //\return $this.prism.workspace.getList().then(function (wsList: vscode.QuickPickItem[]) {
    return prism.workspace.getDetailList().then((detaillist) => {
        function crunchWorkspace(ws, lastPaths) {
            var prefix = lastPaths.map((p) => '   ').join('').replace(/.$/, forkedIcon);
            if (!prefix.trim())
                prefix = "";
            var details = detaillist.find(_ws => _ws.name == ws.name);
            var item = { label: prefix + ws.name };
            if (details) {
                item.description = details.frozen === '1' ? "FROZEN" : "";
            }
            if (ws.name == suggestedWorkspace) {
                item.picked = true;
            }
            //item.detail = lastPaths.join('/');
            var div = { label: '', kind: vscode.QuickPickItemKind.Separator };
            //item.description = lastPaths.join('/');
            if (ws.childs && lastPaths.length == 0) {
                div.label = ws.name;
                masterList.push(div);
            }
            masterList.push(item);
            if (ws.childs) {
                ws.childs.forEach((wsc) => {
                    crunchWorkspace(wsc, lastPaths.concat(ws.name));
                });
                var divEnd = { label: '', kind: vscode.QuickPickItemKind.Separator };
                masterList.push(divEnd);
            }
        }
        return prism.api.execute('listWorkspaces', {}).then((result) => { return JSON.parse(result.workspaces); }).then((nestedList) => {
            try {
                nestedList.forEach((ws) => crunchWorkspace(ws, []));
            }
            catch (e) {
                (0, logger_1.errorMessage)(e);
            }
            return masterList;
        });
    });
}
exports.getWorkspaceChoices = getWorkspaceChoices;
function getFileTypeFilter(wsFiles) {
    var wsfileTypes = {};
    wsFiles.forEach(function (wsf) {
        var [fileExtension] = wsf.match(/[\w\-\_]+\.\w+$/g) || "error";
        wsfileTypes[fileExtension] = fileExtension;
    });
    var __wsFileTypeList = {};
    Object.keys(wsfileTypes).forEach((item) => {
        var dmp = getKurmiDesignMenuStructureFromFilePath(item);
        __wsFileTypeList[dmp] = __wsFileTypeList[dmp] || [];
        __wsFileTypeList[dmp].push(item);
    });
    function folderSort(a, b) {
        const nameA = a.name.toUpperCase();
        const nameB = a.name.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    }
    function camelToTitle(text) {
        const result = text.replace(/([A-Z])/g, " $1");
        return result.charAt(0).toUpperCase() + result.slice(1);
    }
    var fileTypeFiltersChoices = [];
    var ___wsFileTypeListKey = Object.keys(__wsFileTypeList);
    ___wsFileTypeListKey.sort();
    ___wsFileTypeListKey.forEach((folder) => {
        var folderSplit = folder.split(/[\\\/]/);
        /*if (folderSplit.length > 1) {
        }
        folderSplit.forEach((folderPart:string,i:number,arr:string[])=>{
            if (folderPart){
                fileTypeFiltersChoices.push({kind: vscode.QuickPickItemKind.Separator,label: folderPart} as vscode.QuickPickItem);
            }
        })*/
        fileTypeFiltersChoices.push({ kind: vscode.QuickPickItemKind.Separator, label: folder });
        __wsFileTypeList[folder].forEach((folderExtentions) => {
            var label = camelToTitle(folderExtentions).replace(/\.(.+)$/, "");
            var fileExtension = folderExtentions.split('.').pop();
            fileTypeFiltersChoices.push({ label: `${label} ${fileExtension?.toUpperCase()} Files`, description: folderExtentions });
        });
    });
    fileTypeFiltersChoices.splice(0, 0, { label: "Prompt specific files", description: "Choose to present a list of specific files rather than load all files." });
    fileTypeFiltersChoices.splice(1, 0, { kind: vscode.QuickPickItemKind.Separator, label: "File Extension Types" });
    return fileTypeFiltersChoices;
}
exports.getFileTypeFilter = getFileTypeFilter;
function filterWorkspaceFiles(fileTypes = [], wsFilePaths) {
    if (!fileTypes) {
        return wsFilePaths;
    }
    ;
    return wsFilePaths.filter((wsf) => {
        var [fileExtension] = wsf.match(/[\w\-\_]+\.\w+$/g) || "error";
        return fileTypes.indexOf(fileExtension) != -1;
    });
}
exports.filterWorkspaceFiles = filterWorkspaceFiles;
function getWorkspaceFilePaths(workspace, fileTypes = []) {
    return prism.workspace.listWorkspaceFiles(workspace).then(function (wsFilePaths) {
        if (fileTypes.length == 0) {
            return wsFilePaths;
        }
        return filterWorkspaceFiles(fileTypes, wsFilePaths);
    });
}
exports.getWorkspaceFilePaths = getWorkspaceFilePaths;
function chooseSpecificKurmiFilePaths(workspace, wsList) {
    return vscode.window.showQuickPick(wsList.map((label) => ({ label })), { title: `Choose a specific files on ${workspace}`, canPickMany: true }).then((values) => {
        return values?.map(({ label }) => label);
    });
}
exports.chooseSpecificKurmiFilePaths = chooseSpecificKurmiFilePaths;
//# sourceMappingURL=kurmiWorkspaceHelpers.js.map