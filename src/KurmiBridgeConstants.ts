import * as vscode from 'vscode';
export const kurmiappconstants = {
    kurmi: "Kurmi",
    appname: "kurmiwe",
    appDisplayName: "Kurmi Workspace Editor",
    keyFileSystemReady: "fsready",
    keyAppReady: "ready",
    keyKurmiWorkspaceState: "kurmiWorkspaceState",
    keyKurmiLoginData: "kurmiLoginData",
    keyKurmiSessionLocalFSMapping: "kurmiSessionLocalFSMapping",
    keyFileSystemSchemeMode: "fsscheme",
    virtualFileSystemSchemeName: "kurmifs",
    keyKurmiStoredLoginDatas: "LoginDatas",
    keyFileStateData: "fileStateData",
    keyTargetLocalWorkspaceRootDirectory: "targetLocalWorkspaceRootDirectory",
    preferredProgressLocation: vscode.ProgressLocation.Notification,
    keyPreferredProgressLocation: "preferredProgressLocation",
    keyNextTick: "kurmiNextTick",
    popupTimeout: 20000
}
kurmiappconstants.popupTimeout = vscode.workspace.getConfiguration(kurmiappconstants.appname).get('popupTimeout') || 20000;
let preferredProgressLocation = vscode.workspace.getConfiguration(kurmiappconstants.appname).get(kurmiappconstants.keyPreferredProgressLocation);
console.log("preferredProgressLocation", preferredProgressLocation);
kurmiappconstants.preferredProgressLocation = preferredProgressLocation == "Window" ? vscode.ProgressLocation.Window : vscode.ProgressLocation.Notification;
console.log('Constants', kurmiappconstants);