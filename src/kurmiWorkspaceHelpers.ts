import * as vscode from 'vscode';
import { errorMessage, warningMessage } from './logger';
const prism = require('kurmi-prism/prism_onnode.js');
export const whenClauseContext = {
	setReady: function (value: boolean, source: string) {
		console.log("setting ready context to " + value, source);
		vscode.commands.executeCommand('setContext', 'kurmi-pro.ready', value);
	},
	setFSReady: function (value: boolean, source: string) {
		console.log("setting fsready context to " + value, source);
		vscode.commands.executeCommand('setContext', 'kurmi-pro.fsready', value);
	},
	setFSMode: function (context: vscode.ExtensionContext, mode: string) {
		return context.workspaceState.update('kurmi-pro.fsscheme', mode)
	},
	getReady: async function () {
		return await vscode.commands.executeCommand('getContext', 'kurmi-pro.ready') as boolean;
	},
	getFSReady: async function () {
		return await vscode.commands.executeCommand('getContext', 'kurmi-pro.fsready') as boolean;
	},
	getFSMode: function (context: vscode.ExtensionContext) {
		return context.workspaceState.get('kurmi-pro.fsscheme');
	}
}
const pathSeparator = "/"
export function getKurmiDesignMenuStructureFromFilePath(filePath: string): string {
	var ps = pathSeparator;
	var [fileExtension] = filePath.match(/[\w\-\_]+\.\w+$/g) || [''];
	if (fileExtension.length == 0) { return ""; }
	switch (fileExtension.toLowerCase()) {
		case "eventlogger.js": {
			return "Event logger"
		}
		case "apiutil.js": { }
		case "util.js": {
			return "Javascript libraries";
		}
		case "serviceorder.json": { }
		case "service.xml": {
			return `Services${ps}Service Factory`;
		}
		case "inference.js": {
			return `Services${ps}Inference`;
		}
		case "quickfeature.properties": { }
		case "quickfeature.js": { }
		case "quickfeature.xml": {
			return "Quick features";
		}
		case "advancedconnector.xml": { }
		case "connector.properties": { }
		case "connector.xml": { return "Connectors definitions" }
		case "core-entities.xml": { return "User definitions" }
		case "directorymodel.xml": { return "Directory connectors" }
		case "mail.js": { return "Emails" }
		case "rules.json": { }
		case "rulesset.json": { }
		case "ruleset.json": { }
		case "rule.json": { return "Engineering Rules" }
		case "import.js": { }
		case "export.js": { }
		case "componentadjustment.js": { }
		case "discovery.js": { }
		case "dashboardscenario.js": { }
		case "dashboardtenant.js": { }
		case "postprocessing.js": { }
		case "externaltask.js": { }
		case "kurmiapi.js": { }
		case "configuration.js": { }
		case "processing.js": { }
		case "data.json": { }
		case "choice.json": { }
		case "scenario.json": { }
		case "mergeadjust.js": { }
		case "schema.json": { }
		case "options.json": { return "Scenarios and schema" }
		case "widget.js": { return "Homepage widgets" }
		default: {
			warningMessage("Unsupported file extension: ", fileExtension);

			return "";
		}
	}
}
export interface KurmiWorkspaceState {
	title: string;
	step: number;
	totalSteps: number;
	name: string;
	parent?: string;
	filetypes: string[];
	promptSpecificFiles: boolean;
	specificFiles?: string[];
	runtime: vscode.QuickPickItem;
};
export function getWorkspaceChoices(suggestedWorkspace?: string) {

	if (!prism.api.isConnected) {
		throw ["Kurmi is not connected", "Please reconnect."];
	}
	const forkedIcon = "└ ";

	var masterList: vscode.QuickPickItem[] = [];

	//\return $this.prism.workspace.getList().then(function (wsList: vscode.QuickPickItem[]) {
	return prism.workspace.getDetailList().then((detaillist: any[]) => {
		function crunchWorkspace(ws: any, lastPaths: string[]) {
			var prefix = lastPaths.map((p) => '   ').join('').replace(/.$/, forkedIcon);
			if (!prefix.trim()) prefix = "";
			var details = detaillist.find(_ws => _ws.name == ws.name);

			var item: vscode.QuickPickItem = { label: prefix + ws.name };
			if (details) {
				item.description = details.frozen === '1' ? "FROZEN" : "";

			}
			if (ws.name == suggestedWorkspace) {
				item.picked = true;

			}
			//item.detail = lastPaths.join('/');

			var div: vscode.QuickPickItem = { label: '', kind: vscode.QuickPickItemKind.Separator };

			//item.description = lastPaths.join('/');

			if (ws.childs && lastPaths.length == 0) {
				div.label = ws.name;
				masterList.push(div);
			}

			masterList.push(item);
			if (ws.childs) {


				ws.childs.forEach((wsc: any) => {
					crunchWorkspace(wsc, lastPaths.concat(ws.name));
				})
				var divEnd: vscode.QuickPickItem = { label: '', kind: vscode.QuickPickItemKind.Separator };
				masterList.push(divEnd);
			}



		}

		return prism.api.execute('listWorkspaces', {}).then((result: any) => { return JSON.parse(result.workspaces) }).then((nestedList: any[]) => {
			try {
				nestedList.forEach((ws: any) => crunchWorkspace(ws, []));
			}
			catch (e: any) {
				errorMessage(e);
			}
			return masterList;
		});
	})
}

export function getFileTypeFilter(wsFiles: string[]) {

	var wsfileTypes: any = {};
	wsFiles.forEach(function (wsf: string) {
		var [fileExtension] = wsf.match(/[\w\-\_]+\.\w+$/g) || "error";
		wsfileTypes[fileExtension] = fileExtension;
	});
	var __wsFileTypeList: any = {};
	Object.keys(wsfileTypes).forEach((item: string) => {
		var dmp = getKurmiDesignMenuStructureFromFilePath(item);
		__wsFileTypeList[dmp] = __wsFileTypeList[dmp] || [];
		__wsFileTypeList[dmp].push(item);
	});

	function folderSort(a: any, b: any) {
		const nameA = a.name.toUpperCase();
		const nameB = a.name.toUpperCase();
		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}
		return 0;
	}
	function camelToTitle(text: string) {
		const result = text.replace(/([A-Z])/g, " $1");
		return result.charAt(0).toUpperCase() + result.slice(1);
	}

	var fileTypeFiltersChoices: vscode.QuickPickItem[] = [];
	var ___wsFileTypeListKey = Object.keys(__wsFileTypeList);
	___wsFileTypeListKey.sort();
	___wsFileTypeListKey.forEach((folder: string) => {
		var folderSplit = folder.split(/[\\\/]/);
		/*if (folderSplit.length > 1) {
		}
		folderSplit.forEach((folderPart:string,i:number,arr:string[])=>{
			if (folderPart){
				fileTypeFiltersChoices.push({kind: vscode.QuickPickItemKind.Separator,label: folderPart} as vscode.QuickPickItem);
			}
		})*/
		fileTypeFiltersChoices.push({ kind: vscode.QuickPickItemKind.Separator, label: folder } as vscode.QuickPickItem);

		__wsFileTypeList[folder].forEach((folderExtentions: string) => {
			var label = camelToTitle(folderExtentions).replace(/\.(.+)$/, "");
			var fileExtension = folderExtentions.split('.').pop();
			fileTypeFiltersChoices.push({ label: `${label} ${fileExtension?.toUpperCase()} Files`, description: folderExtentions } as vscode.QuickPickItem);
		})
	})

	fileTypeFiltersChoices.splice(0, 0, { label: "Prompt specific files", description: "Choose to present a list of specific files rather than load all files." } as vscode.QuickPickItem);
	fileTypeFiltersChoices.splice(1, 0, { kind: vscode.QuickPickItemKind.Separator, label: "File Extension Types" } as vscode.QuickPickItem);
	return fileTypeFiltersChoices;
}
export function filterWorkspaceFiles(fileTypes: string[] = [], wsFilePaths: string[]) {
	if (!fileTypes) { return wsFilePaths };
	return wsFilePaths.filter((wsf: any) => {
		var [fileExtension] = wsf.match(/[\w\-\_]+\.\w+$/g) || "error";
		return fileTypes.indexOf(fileExtension) != -1;
	});
}
export function getWorkspaceFilePaths(workspace: string, fileTypes: string[] = []) {

	return prism.workspace.listWorkspaceFiles(workspace).then(function (wsFilePaths: string[]) {
		if (fileTypes.length == 0) {
			return wsFilePaths;
		}
		return filterWorkspaceFiles(fileTypes, wsFilePaths);
	})
}
export function chooseSpecificKurmiFilePaths(workspace: string, wsList: string[]) {
	return vscode.window.showQuickPick(wsList.map((label) => ({ label })), { title: `Choose a specific files on ${workspace}`, canPickMany: true }).then((values) => {
		return values?.map(({ label }) => label);
	});
}
