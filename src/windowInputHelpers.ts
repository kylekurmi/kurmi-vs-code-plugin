

import { QuickPickItem, QuickPickItemButtonEvent, window, Disposable, CancellationToken, ThemeIcon, QuickInputButton, QuickInput, ExtensionContext, QuickInputButtons, Uri } from 'vscode';
class InputFlowAction {
	static back = new InputFlowAction();
	static cancel = new InputFlowAction();
	static resume = new InputFlowAction();
}
type InputStep = (input: MultiStepInput) => Thenable<InputStep | void>;
interface QuickInputExtented<T extends QuickInput> {
	onDidTriggerButton?: (e: QuickPickItemButtonEvent<any>) => any;
}

interface QuickPickParameters<T extends QuickPickItem> {
	title: string;
	step: number;
	totalSteps: number;
	items: T[];
	activeItems?: T[];
	ignoreFocusOut?: boolean;
	placeholder: string;
	buttons?: QuickInputButton[];
	canSelectMany?: boolean;
	onDidTriggerItemButton?: (e: QuickPickItemButtonEvent<T>) => void;
	onDidTriggerButton?: (e: QuickPickItemButtonEvent<T>) => void;
	shouldResume: () => Thenable<boolean>;
}
interface InputBoxParameters {
	title: string;
	step: number;
	totalSteps: number;
	value: string;
	prompt: string;
	validate: (value: string) => Promise<string | undefined>;
	buttons?: QuickInputButton[];
	ignoreFocusOut?: boolean;
	placeholder?: string;
	shouldResume: () => Thenable<boolean>;
}
export class MultiStepInput {
	registerDisposibleTrigger(triggerEvent: Thenable<any>) {
		var $this = this;
		triggerEvent.then((e) => {
			var currentInput = $this.current;
			if (currentInput) {
				currentInput.dispose();
			}
		});
	}
	getCurrentInput() {
		return this.current;
	}
	static async run<T>(start: InputStep) {
		const input = new MultiStepInput();

		return input.stepThrough(start);
	}
	private current?: QuickInput;
	private steps: InputStep[] = [];
	private async stepThrough<T>(start: InputStep) {
		let step: InputStep | void = start;
		while (step) {
			this.steps.push(step);
			if (this.current) {
				this.current.enabled = false;
				this.current.busy = true;
			}
			try {
				step = await step(this);
			} catch (err) {
				if (err === InputFlowAction.back) {
					this.steps.pop();
					step = this.steps.pop();
				} else if (err === InputFlowAction.resume) {
					step = this.steps.pop();
				} else if (err === InputFlowAction.cancel) {
					step = undefined;
				} else {
					throw err;
				}
			}
		}
		if (this.current) {
			this.current.dispose();
		}
	}
	async showQuickPick<T extends QuickPickItem, P extends QuickPickParameters<T>>({ title, step, totalSteps, items, canSelectMany, activeItems, onDidTriggerItemButton, ignoreFocusOut, placeholder, buttons, shouldResume }: P) {
		const disposables: Disposable[] = [];
		try {

			return await new Promise<P extends { canSelectMany: true } ? QuickPickItem[] : QuickPickItem | (P extends { buttons: (infer I)[] } ? I : never)>((resolve, reject) => {
				const input = window.createQuickPick<T>();
				input.title = title;
				input.step = step;
				input.totalSteps = totalSteps;
				input.ignoreFocusOut = ignoreFocusOut ?? false;
				input.placeholder = placeholder;
				input.items = items;
				input.canSelectMany = canSelectMany ?? false;

				if (activeItems) {
					input.activeItems = activeItems || [];
				}
				input.buttons = [
					...(this.steps.length > 1 ? [QuickInputButtons.Back] : []),
					...(buttons || [])
				];

				disposables.push(
					input.onDidTriggerButton(item => {
						if (item === QuickInputButtons.Back) {
							reject(InputFlowAction.back);
						} else {
							resolve(canSelectMany ? item : [item] as any);
						}
					}),

					input.onDidAccept(() => {
						var items = input.selectedItems;
						resolve(canSelectMany ? items : items[0] as any);
					}),
					//input.onDidChangeSelection(items => resolve(canSelectMany ? items : items[0] as any)),
					input.onDidHide(() => {
						(async () => {
							let doReject = shouldResume && await shouldResume();
							reject(doReject ? InputFlowAction.resume : InputFlowAction.cancel);
						})()
							.catch(reject);
					})
				);
				if (onDidTriggerItemButton) {
					disposables.push(input.onDidTriggerItemButton(e => {
						resolve(e.item as any);
						onDidTriggerItemButton(e);
					}));
				}

				if (this.current) {
					this.current.dispose();
				}
				this.current = input;
				this.current.show();
			});

		} finally {
			disposables.forEach(d => d.dispose());
		}
	}

	async showInputBox<P extends InputBoxParameters>({ title, step, totalSteps, value, prompt, validate, buttons, ignoreFocusOut, placeholder, shouldResume }: P) {
		const disposables: Disposable[] = [];
		try {
			return await new Promise<string | (P extends { buttons: (infer I)[] } ? I : never)>((resolve, reject) => {
				const input = window.createInputBox();
				input.title = title;
				input.step = step;
				input.totalSteps = totalSteps;
				input.value = value || '';
				input.prompt = prompt;
				input.ignoreFocusOut = ignoreFocusOut ?? false;
				input.placeholder = placeholder;
				input.buttons = [
					...(this.steps.length > 1 ? [QuickInputButtons.Back] : []),
					...(buttons || [])
				];
				let validating = validate('');
				disposables.push(
					input.onDidTriggerButton(item => {
						if (item === QuickInputButtons.Back) {
							reject(InputFlowAction.back);
						} else {
							resolve(<any>item);
						}
					}),
					input.onDidAccept(async () => {
						const value = input.value;
						input.enabled = false;
						input.busy = true;
						if (!(await validate(value))) {
							resolve(value);
						}
						input.enabled = true;
						input.busy = false;
					}),
					input.onDidChangeValue(async text => {
						const current = validate(text);
						validating = current;
						const validationMessage = await current;
						if (current === validating) {
							input.validationMessage = validationMessage;
						}
					}),
					input.onDidHide(() => {
						(async () => {
							reject(shouldResume && await shouldResume() ? InputFlowAction.resume : InputFlowAction.cancel);
						})()
							.catch(reject);
					})
				);
				if (this.current) {
					this.current.dispose();
				}
				this.current = input;
				this.current.show();
			});
		} finally {
			disposables.forEach(d => d.dispose());
		}
	}
}