import * as vscode from 'vscode';
import { kurmiappconstants } from './KurmiBridgeConstants';
const outputLogger: vscode.LogOutputChannel = vscode.window.createOutputChannel("kurmi-bridge", { log: true });
outputLogger.show();
export function errorMessage(title: string, e: any = '') {
  let message = e ? "\n" + (typeof e == "string" ? e : JSON.stringify(e)) : "";
  outputLogger.error("[" + title + "] " + message);
  console.error("[" + title + "] " + message);
  vscode.window.showErrorMessage([title].concat(message).join(' - '));
}
export function infoMessage(title: string, e: any = '') {
  let message = e ? "\n" + (typeof e == "string" ? e : JSON.stringify(e)) : "";
  vscode.window.withProgress(
    {
      location: kurmiappconstants.preferredProgressLocation,
      title: kurmiappconstants.appDisplayName,
      cancellable: false,
    },
    async (progress, token) => {
      for (let i = 0; i < 10; i++) {
        setTimeout(() => {
          progress.report({ increment: i * 10, message: [title].concat(message).join(' - ') })
        }, kurmiappconstants.popupTimeout || 10000)
      }
    }
  )
  outputLogger.info("[" + title + "] " + JSON.stringify(message));
  console.log("[" + title + "] " + JSON.stringify(message));
}
export function warningMessage(title: string, message: any = '') {
  outputLogger.warn("[" + title + "] " + JSON.stringify(message));
  console.warn("[" + title + "] " + JSON.stringify(message));
}