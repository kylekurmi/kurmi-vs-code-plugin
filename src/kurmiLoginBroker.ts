/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
import { QuickPickItem, CancellationToken, ThemeIcon, QuickInputButton, ExtensionContext, Uri } from 'vscode';
import * as vscode from 'vscode';
import { errorMessage, infoMessage } from './logger';
import { whenClauseContext, getFileTypeFilter, getWorkspaceChoices, chooseSpecificKurmiFilePaths, KurmiWorkspaceState, filterWorkspaceFiles } from './kurmiWorkspaceHelpers';
import { MultiStepInput } from './windowInputHelpers';
import { kurmiappconstants } from './KurmiBridgeConstants';
import { KurmiWorkspaceEditor } from './kurmiVirtualWorkspace';
var url = require('url');
const prism = require('kurmi-prism/prism_onnode.js');
/**
 * A multi-step input using window.createQuickPick() and window.createInputBox().
 * 
 * This first part uses the helper class `MultiStepInput` that wraps the API for the multi-step case.
 */
export interface KurmiLoginState {
	title: string;
	step: number;
	totalSteps: number;
	resourceGroup: QuickPickItem | string;
	name: string;
	hostName: string,
	login: string,
	password: string,
	lastLogin: Date,
	knickname: string
	saveData: boolean,
	runtime: QuickPickItem;
}
export async function getStoredLoginDatas(context: vscode.ExtensionContext) {
	let loginDatas: any[] = [];
	var rawState = await context.secrets.get(kurmiappconstants.keyKurmiStoredLoginDatas);
	if (rawState) {
		var retrievedLoginDatas: any[];
		try {
			retrievedLoginDatas = [].concat(JSON.parse(rawState || "[]"));
		}
		catch (e) {
			console.error(e);
			retrievedLoginDatas = [];
		}
		loginDatas.splice(0, 0, ...retrievedLoginDatas);
		loginDatas.sort((a: any, b: any) => { return (a.lastLogin - b.lastLogin); });
	}
	return loginDatas;
}
export async function kurmiLogin(passibleData: KurmiWorkspaceEditor.VSCodeWorkingData) {
	whenClauseContext.setReady(false, "kurmiLogin");
	var loginData: any;
	if (!passibleData.token) throw "No Token";
	return await prismLoginBroker(passibleData.context, passibleData.token).then(async (state) => {
		loginData = state;
		passibleData.progress.report({ message: "Loading Kurmi Prism" });
		await prism.api.login(loginData.login, loginData.password, `${loginData.hostName}`);
		passibleData.progress.report({ message: "Trying to connect to platform." });
		try {
			return await prism.api.connect();
		}
		catch (e) {
			throw e;
		}
	}).then((e) => {
		passibleData.progress.report({ message: `Connected to Kurmi Platform (${loginData.hostName}) VERSION: ${e.kurmiVersion}` });
		vscode.window.setStatusBarMessage(`Connected to Kurmi Platform (${loginData.hostName}) VERSION: ${e.kurmiVersion}`)
		whenClauseContext.setReady(true, "then prism.api.connect");
		return loginData;
	}).catch((e) => {
		//progress.report({ message: `Connected to Kurmi Platform (${loginData.hostName}) VERSION: ${e.kurmiVersion} ` });
		console.error(e);
		var MO: vscode.MessageOptions = { modal: true };
		vscode.window.showErrorMessage(`Failed to connect to Kurmi Platform ${loginData.knickname || loginData.hostName}`, MO);
		whenClauseContext.setReady(false, "catch prism.api.connect");
		throw e;
	});
};
async function prismLoginBroker(context: ExtensionContext, token: CancellationToken) {
	class MyButton implements QuickInputButton {
		constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
	}
	async function collectInputs() {
		const state = {} as Partial<KurmiLoginState>;
		await MultiStepInput.run(input => inputHostName(input, state));
		return state as KurmiLoginState;
	}
	const title = 'Kurmi Platform Authentication';
	async function inputHostName(input: MultiStepInput, state: Partial<KurmiLoginState>, callback?: Function) {
		if (token.isCancellationRequested) {
			return;
		}
		const additionalSteps = 0;
		// TODO: Remember currently active item when navigating back.
		var urlString = await input.showInputBox({
			title,
			step: 1 + additionalSteps,
			prompt: 'Enter url',
			value: '',
			ignoreFocusOut: true,
			validate: validateHostname,
			totalSteps: 3 + additionalSteps,
			placeholder: "https://<kurmi.domain.com>",
			shouldResume: shouldResume
		});
		var parsedURL = url.parse(urlString);
		state.hostName = `${parsedURL.protocol}//${parsedURL.host}`;
		return (input: MultiStepInput) => (callback && callback(input, state)) || inputLogin(input, state);
	}
	async function inputLogin(input: MultiStepInput, state: Partial<KurmiLoginState>) {
		const additionalSteps = 0;
		if (token.isCancellationRequested) {
			return;
		}
		// TODO: Remember currently active item when navigating back.
		state.login = await input.showInputBox({
			title,
			step: 2 + additionalSteps,
			prompt: `Enter user for [${state.hostName}]`,
			value: 'admin',
			ignoreFocusOut: true,
			validate: validateLoginPassword,
			totalSteps: 3 + additionalSteps,
			placeholder: "admin",
			shouldResume: shouldResume
		});
		return (input: MultiStepInput) => inputPassword(input, state);
	}
	async function inputPassword(input: MultiStepInput, state: Partial<KurmiLoginState>) {
		const additionalSteps = 0;
		if (token.isCancellationRequested) {
			return;
		}
		// TODO: Remember currently active item when navigating back.
		state.password = await input.showInputBox({
			title,
			step: 3 + additionalSteps,
			prompt: `Enter password for login[${state.login}]=>[${state.hostName}]`,
			value: 'admin',
			ignoreFocusOut: true,
			validate: validateLoginPassword,
			totalSteps: 3 + additionalSteps,
			placeholder: "admin",
			shouldResume: shouldResume
		});
		return (input: MultiStepInput) => chooseSaveData(input, state);
	}
	async function chooseSaveData(input: MultiStepInput, state: Partial<KurmiLoginState>) {
		const additionalSteps = 0;
		const saveData: QuickPickItem[] = ['Save', 'Forget'].map((label) => ({ label }));
		// TODO: Remember currently active item when navigating back.
		let saveDataAnswer = (await input.showQuickPick({
			title: "Remember this information?",
			step: 4 + additionalSteps,
			items: saveData,
			totalSteps: 4 + additionalSteps,
			placeholder: "Save",
			ignoreFocusOut: true,
			shouldResume: shouldResume
		})).label === "Save";
		state.saveData = saveDataAnswer;
		if (saveDataAnswer) {
			return (input: MultiStepInput) => chooseKnickName(input, state);
		}
	}
	async function chooseKnickName(input: MultiStepInput, state: Partial<KurmiLoginState>) {
		const additionalSteps = 0;
		// TODO: Remember currently active item when navigating back.
		let saveKickName = (await input.showInputBox({
			title,
			prompt: "Enter an optional knickname for this connection.",
			step: 5 + additionalSteps,
			placeholder: 'my kurmiblab',
			totalSteps: 5 + additionalSteps,
			ignoreFocusOut: true,
			validate: validateLoginPassword,
			shouldResume: shouldResume,
			value: ''
		}))
		state.knickname = saveKickName;
	}
	function shouldResume() {
		// Could show a notification with the option to resume.
		return new Promise<boolean>((resolve, reject) => {
			// noop
		});
	}
	async function validateHostname(name: string) {
		var parsedURL = url.parse(name);
		if (parsedURL.protocol && parsedURL.host) {
			return undefined;
		}
		else if (!parsedURL.protocol) {
			return 'Missing protocol (https://)';
		}
		else if (!parsedURL.host) {
			return "Missing host";
		}
		return undefined;
	}
	async function validateLoginPassword(name: string) {
		return undefined;
	}
	async function validateNameIsUnique(name: string) {
		// ...validate...
		await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}
	var returnKurmiLoginState = new Promise((resolve, reject) => {
		(async () => {
			if (token.isCancellationRequested) {
				throw "Cancelled!"
			}
			token.onCancellationRequested((e) => {
				throw "Cancelled!"
			})
			var contextLoginData = context.workspaceState.get(kurmiappconstants.keyKurmiLoginData);
			if (contextLoginData) {
				return contextLoginData;
			}
			let loginDatas = await getStoredLoginDatas(context);
			loginDatas.sort((a: KurmiLoginState, b: KurmiLoginState) => {
				let bdate = (new Date(b.lastLogin));
				let adate = new Date(a.lastLogin);
				return bdate.getTime() - adate.getTime();
			});
			var chooseLoginDataItems: QuickPickItem[] = loginDatas.map((ld) => {
				let QPI: QuickPickItem = { alwaysShow: true, label: ld.knickname || ld.hostName, detail: ld.hostName, description: `login: ${ld.login}` };
				return QPI;
			});
			chooseLoginDataItems.push({ label: 'Enter a new host' } as QuickPickItem)
			var savedLoginData = await vscode.window.showQuickPick(chooseLoginDataItems, { title: "Choose a saved kurmi platform" });
			if (savedLoginData) {
				var foundLoginData = loginDatas.find((ld) => ld.hostName === savedLoginData?.detail);
				if (foundLoginData) {
					foundLoginData.lastLogin = new Date();
					context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, foundLoginData);
					return foundLoginData;
				}
			}
			const state = await collectInputs();
			if (state.saveData) {
				loginDatas.push(state);
				context.secrets.store(kurmiappconstants.keyKurmiStoredLoginDatas, JSON.stringify(loginDatas));
			}
			state.lastLogin = new Date();
			context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, state);
			return state;
		})().then((state) => {
			resolve(state);
		})
	});
	return returnKurmiLoginState;
}
class MyButton implements QuickInputButton {
	constructor(public iconPath: vscode.ThemeIcon, public tooltip: string) { }
}
export class kurmiWorkspaceBroker {
	disposeListener: any;
	title: string;
	state: Partial<KurmiWorkspaceState>;
	pathSeparator: string;
	save: Function;
	private context: ExtensionContext;
	private progress: any;
	private token?: vscode.CancellationToken;
	private mode?: KurmiWorkspaceEditor.KurmiEditorMode;
	constructor(passibleData: KurmiWorkspaceEditor.VSCodeWorkingData) {
		this.context = passibleData.context;
		this.title = 'Kurmi Workspace';
		this.pathSeparator = "/";
		this.token = passibleData.token;
		this.progress = passibleData.progress;
		this.state = (passibleData.context.workspaceState.get(kurmiappconstants.keyKurmiWorkspaceState) || {}) as Partial<KurmiWorkspaceState>;
		this.mode = passibleData.mode;
		this.save = function () {
			return passibleData.context.workspaceState.update(kurmiappconstants.keyKurmiWorkspaceState, this.state);
		}
	}
	async initialize(passibleData: Partial<KurmiWorkspaceEditor.VSCodeWorkingData>): Promise<KurmiWorkspaceState> {
		return new Promise((resolve, reject) => {
			(async () => {
				if (this.token) {
					this.token.onCancellationRequested((e: any) => {
						reject(e);
					});
				}
				this.progress.report({ message: "Collecting Workspace information" });
				return await this.collectInputs(passibleData.mode == KurmiWorkspaceEditor.KurmiEditorMode.silent);
			})().then((state) => {
				this.state = state;
				this.save();
				resolve(state);
			}).catch((e) => {
				errorMessage('Workspace Collect Failed', e);
			})
		})
	}
	async chooseFileTypes(input: MultiStepInput, _state?: Partial<KurmiWorkspaceState>) {
		let state: Partial<KurmiWorkspaceState> = _state || this.state;
		let $progress = this.progress;
		$progress.report({ message: "Retrieving specific file types to load" });
		state.specificFiles = await prism.workspace.listWorkspaceFiles(state.name) || [];
		if (this.mode != KurmiWorkspaceEditor.KurmiEditorMode.clone) {
			const additionalSteps = 0;
			if (this.disposeListener) {
				input.registerDisposibleTrigger(this.disposeListener);
			}
			// TODO: Remember currently active item when navigating back.
			const PromptSpecificFilesButton = new MyButton(new ThemeIcon("search-view-icon"), 'Open specific files');
			$progress.report({ message: "Choosing specific file types to load" });
			var fileTypes: any[] = await input.showQuickPick({
				title: this.title,
				step: 3 + additionalSteps,
				items: getFileTypeFilter(state.specificFiles || []),
				canSelectMany: true,
				ignoreFocusOut: false,
				validate: this.validateLoginPassword,
				totalSteps: 3 + additionalSteps,
				placeholder: "admin",
				shouldResume: this.shouldResume,
				onDidTriggerButton: (e) => {
					if (e.button == PromptSpecificFilesButton) {
						state.promptSpecificFiles = true;
					}
				},
				buttons: [PromptSpecificFilesButton]
			});
			var promptSpecificFilesIndex = fileTypes.findIndex(f => f.label == "Prompt specific files")
			if (promptSpecificFilesIndex !== -1) {
				state.promptSpecificFiles = true;
				fileTypes.splice(promptSpecificFilesIndex, 1);
			}
			if (fileTypes.length) {
				state.filetypes = fileTypes.map(ft => ft.description);
			}

			this.save();
			return (input: MultiStepInput) => this.chooseSpecificFiles(input, state);
		}

	}
	async chooseSpecificFiles(input: MultiStepInput, _state?: Partial<KurmiWorkspaceState>) {
		let state: Partial<KurmiWorkspaceState> = _state || this.state;
		let $progress = this.progress;
		const additionalSteps = 1;
		$progress.report({ message: "Filtering specific file types to load" });
		state.specificFiles = filterWorkspaceFiles(state.filetypes, state.specificFiles || []);
		if (state.promptSpecificFiles === true) {
			$progress.report({ message: "Choosing specific files to load" });
			state.specificFiles = await chooseSpecificKurmiFilePaths(state.name || '', state.specificFiles || []);
			$progress.report({ message: `Loading specific ${state.specificFiles?.length} files by type` });
		}
		else {
			infoMessage("Loading all files in the filter", `${state.specificFiles.length} files`)
			$progress.report({ message: `Loading ${state.specificFiles.length} files` });
		}
		// TODO: Remember currently active item when navigating back.

		this.save();
	}
	private shouldResume() {
		// Could show a notification with the option to resume.
		return new Promise<boolean>((resolve, reject) => { reject(false); });
	}
	private async validateHostname(name: string) {
		var parsedURL = url.parse(name);
		if (parsedURL.protocol && parsedURL.host) {
			return undefined;
		}
		else if (!parsedURL.protocol) {
			return 'Missing protocol (https://)';
		}
		else if (!parsedURL.host) {
			return "Missing host";
		}
		return undefined;
	}
	private async validateLoginPassword(name: string) {
		return undefined;
	}
	private async validateNameIsUnique(name: string) {
		// ...validate...
		await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}
	private async collectInputs(silentMode?: boolean) {
		const state = this.state;
		if (silentMode !== true) {
			await MultiStepInput.run(input => this.chooseWorkspace(input, state));
		}
		else {
		}
		return state as KurmiWorkspaceState;
	}
	async chooseWorkspace(input: MultiStepInput, _state?: Partial<KurmiWorkspaceState>, callback?: Function) {
		let state: Partial<KurmiWorkspaceState> = _state || this.state;
		if (this.disposeListener) {
			input.registerDisposibleTrigger(this.disposeListener);
		}
		const BranchButton = new MyButton(new ThemeIcon("git-branch-create"), 'Create Branch of this workspace');
		this.progress.report({ message: "Retrieving workspace list" });
		const additionalSteps = 0;
		var items = await getWorkspaceChoices(state.name);
		items.forEach((item: QuickPickItem) => {
			item.buttons = [BranchButton];
		})
		this.progress.report({ message: "Choosing workspace" });
		// TODO: Remember currently active item when navigating back.
		let suggested = (items.filter((i: QuickPickItem) => i.picked)[0] || { label: 'kurmi-builtin' }).label.replace("└", "").trim();
		var workspace = await input.showQuickPick({
			title: this.title,
			step: 1 + additionalSteps,
			items: items,
			activeItems: items.filter((i: QuickPickItem) => i.picked),
			validate: (() => { }),
			ignoreFocusOut: true,
			totalSteps: 3 + additionalSteps,
			placeholder: suggested,
			shouldResume: this.shouldResume,
			onDidTriggerItemButton: (e) => {
				state.parent = e.item.label.replace("└", "").trim();
			}
		});
		state.name = workspace.label.replace("└", "").trim();
		this.title = "Workspace " + state.name;
		this.save();
		return (input: MultiStepInput) => callback ? callback(input, state) : this.inputBranchName(input, state);
	}
	async inputBranchName(input: MultiStepInput, _state?: Partial<KurmiWorkspaceState>) {
		let state: Partial<KurmiWorkspaceState> = _state || this.state;
		const additionalSteps = state.parent ? 1 : 0;
		if (this.disposeListener) {
			input.registerDisposibleTrigger(this.disposeListener);
		}
		// TODO: Remember currently active item when navigating back.
		if (this.state.parent) {
			this.progress.report({ message: "Choosing a new branch" });
			state.name = await input.showInputBox({
				title: this.title,
				step: 2 + additionalSteps,
				ignoreFocusOut: true,
				prompt: `Enter the branch name for ${state.parent}`,
				value: `${state.parent}_${(new Date()).toUTCString().replace(/\D/g, "").substring(0, 8)}`,
				validate: this.validateLoginPassword,
				totalSteps: 3 + additionalSteps,
				placeholder: `${state.parent}_branch`,
				shouldResume: this.shouldResume
			});
			try {
				this.progress.report({ message: "Creating the new branch" });
				await prism.workspace.createBranch(state.name, state.parent);
			}
			catch (e) {
				this.progress.report({ message: "Failed to create a new branch" });
				errorMessage(`Failed to create branch of workspace ${state.name} of ${state.parent}`, e)
				throw e;
			}
			this.save();
		}
		return (input: MultiStepInput) => this.chooseFileTypes(input, state);
	}
}
