import * as vscode from 'vscode';
import * as fs from 'fs'
import { errorMessage, infoMessage, warningMessage } from './logger';
import { KurmiLoginState, kurmiWorkspaceBroker } from './kurmiLoginBroker';
import { kurmiappconstants } from './KurmiBridgeConstants';
import path = require('path');
const forwardslash = "/";
const prism = require('kurmi-prism/prism_onnode.js');
export enum KurmiFileSystemEmulation {
	likeConfigFile = "Like configfile.zip", likeDesignMenu = "Like design menu", flat = "flat structure"
}
export interface KurmiVirtualWorkspaceOptions {
	virtual: boolean;
	targetWorkspaceName?: string;
	emulationType?: KurmiFileSystemEmulation;
	progress: any;
	token: vscode.CancellationToken;
	silentMode?: boolean;
}
export namespace KurmiWorkspaceEditor {
	export interface KurmiPlaftormWorkspaceFilePath {
		loginData: KurmiLoginState;
		workspaceName: string;
		filePath: string;
	}
	export interface VSCodeWorkingData {
		context: vscode.ExtensionContext;
		progress?: any;
		token?: vscode.CancellationToken;
		mode: KurmiEditorMode;
	}
	export enum KurmiEditorMode {
		silent, verbose, clone, pull
	}
	export class VSCodeExtensionDataManager implements VSCodeWorkingData {
		connections: Connection[] = [];
		context: vscode.ExtensionContext;
		progress?: any;
		token?: vscode.CancellationToken;
		mode: KurmiEditorMode = KurmiEditorMode.silent;
		currentSession?: KurmiSessionState;
		sessionLocalFSMapping: any;
		emulationType: KurmiFileSystemEmulation = KurmiFileSystemEmulation.likeConfigFile;
		prism: any;
		constructor(context: vscode.ExtensionContext, prism: any) {
			this.context = context;
			this.prism = prism;
			//this.progress = vws.progress
			//this.token = vws.token;
			//this.mode = vws.mode;
			this.sessionLocalFSMapping = this.context.workspaceState.get(kurmiappconstants.keyKurmiSessionLocalFSMapping) || {};
		}
		getKurmiFileSimulatedUri(workspace: string, kurmiFilePath: string) {
			return vscode.Uri.parse(`${kurmiappconstants.appname}://${this.currentSession?.connection.host.authority}/${workspace}${kurmiFilePath}`);
		}
		getUri(paths: string[]) {
			if (this.currentSession && this.currentSession.connection.baseuri) {
				return vscode.Uri.joinPath(this.currentSession.connection.baseuri, ...paths);
			}
			else {
				throw "No current session!";
			}
		}
		createFolders(workspace: string, folderList: string[]) {
			return Promise.all(folderList.map(folder => this.getUri([workspace, folder])).map(vscode.workspace.fs.createDirectory));
		}
		fetch(workspace: string): string[] {
			return prism.workspace.listWorkspaceFiles(workspace);
		}
		pull(workspace: string) {
			let $this = this;
			let files = this.currentSession?.fileQueue || [];
			let folders = this.getFoldersList(files);
			this.createFolders(workspace, folders);
			return vscode.window.withProgress({ title: `Getting from ${workspace}`, location: kurmiappconstants.preferredProgressLocation, cancellable: true },
				async (progress, token) => {
					await Promise.all(files.map(function (filePath, i: number, arr: string[]) {
						if (token.isCancellationRequested === true) {
							return;
						}
						else {
							return $this.pullFile(workspace, filePath).then((createFileResult: any) => {
								progress.report({ message: `${filePath}`, increment: 100 * (1 / arr.length) });
								return createFileResult;
							});
						}
					}));
				});
		}
		pullFile(workspace: string, kurmiFilePath: string) {
			let $this = this;
			let filePath = kurmiFilePath.replace(/^[\\\/]+/, "");
			let storageUri = $this.getUri([workspace, filePath]);
			let storagePath = storageUri.path;
			var storageData: any = {};
			//$this.fileStateData.set(storageUri,storageData);
			return this.prism.workspace.getWorkspaceFile(workspace, kurmiFilePath).then(async function (fileResult: FileResult) {
				if (fileResult) {

					let exists = fs.existsSync(storageUri.fsPath);
					var localDoc = exists && await vscode.workspace.openTextDocument(storageUri);
					if (exists && localDoc && localDoc.getText() != fileResult.content) {
						var serverDoc = await vscode.workspace.openTextDocument({ content: fileResult.content });
						vscode.commands.executeCommand("vscode.diff", storageUri, serverDoc.uri);

					}
					else {
						vscode.workspace.fs.writeFile(storageUri, Buffer.from(fileResult.content || ''));
					}
					delete fileResult.content;
					let cs = $this.currentSession;
					//$this.currentSession?.managedFileData.set(storageUri,fileResult);
					var data: Partial<KurmiWorkspaceEditor.FileResult> = { ...fileResult };
					if (data.content) {
						delete data.content;
					}
					if (cs) {
						data.originHostName = cs && cs.connection.baseuri?.authority;
						data.workspace = cs && cs.workspaceName;
					}
					else {
						throw "NO CS... COMMIT"
					}
					cs?.managedFileData.set(storageUri, data);
				}
				else {
					throw `File content not found for ${$this.getKurmiFileSimulatedUri(workspace, kurmiFilePath)}`;
				}
				console.log(`Fetched ${$this.getKurmiFileSimulatedUri(workspace, kurmiFilePath)}`);
			}).catch(function (e: any) {
				vscode.workspace.fs.writeFile(storageUri, Buffer.from(JSON.stringify(e)));
				errorMessage('createVirtualFile', e);
			});
		}
		renameFile(oldUri: vscode.Uri, newUri: vscode.Uri) {
			let $this = this;
			var data: any = this.currentSession?.managedFileData.get(oldUri);
			this.currentSession?.managedFileData.set(newUri, data);
			this.currentSession?.managedFileData.set(oldUri, undefined);
			infoMessage("renameFile", "Updating the fileStateData and saving as a new file");
			var doc = vscode.workspace.textDocuments.find(f => f.uri == newUri);
			if (doc != undefined) {
				this.commitFile(doc).then((res) => {
					infoMessage("renameFile", "Deleting old kurmi file now that the new one is saved.");
					this.deleteFiles([oldUri]);
				}).catch((e) => {
					errorMessage("renameFile", `Didnt save the new file to allow the deletion of the other;`);
				});
			}
			else {
				errorMessage("renameFile", "Didnt find the file in vscode workspace.");
			}
		}
		deleteFiles(files: vscode.Uri[]) {
			let $this = this;
			let fileDatas = files.map(f => this.currentSession?.managedFileData.get(f));
			var kurmiPaths = fileDatas.map(({ path }) => path);
			return this.prism.api.execute('deleteWorkspaceFiles', { workspace: this.currentSession?.workspaceName, path: kurmiPaths }).then(function (res: any) {
				var mo: vscode.MessageOptions = {};
				mo.modal = true;
				mo.detail = `File Deleted at \r\n\t${fileDatas.map(fd => $this.getKurmiFileSimulatedUri(fd.workspace, fd.path)).join('\t\r\n')}\r\n (${JSON.stringify(res)})`;
				vscode.window.showInformationMessage("Success", mo);
			}, function (e: any) {
				errorMessage('deleteFiles', e);
			});
		}
		async commitFile(e: vscode.TextDocument) {
			let cs = this.currentSession;
			if (!cs) {
				errorMessage("saveFile", "No Current Session");
				throw new Error("saveFile() No Current Session");
			}
			var data: any = this.currentSession?.managedFileData.get(e.uri);
			if (!data.path) {
				await vscode.window.showInputBox({ prompt: `Enter the path on ws ${cs.workspaceName}`, placeHolder: e.uri.path, title: "Save new file on workspace" })
					.then((newPath: string | undefined) => {
						data.path = newPath || '';
					});
			}
			else {
				var serverVersion = await this.prism.workspace.getWorkspaceFile(cs.workspaceName, data.path);
				//vscode.commands.executeCommand('vscode.diff',{left:JSON.parse(serverVersion.content),right:e.getText(),title:"Server Version | Local Version"});
				var serverIsNewer = serverVersion.revisionDate !== data.revisionDate;
				if (serverIsNewer) {
					var serverDoc = await vscode.workspace.openTextDocument({ content: serverVersion.content });
					vscode.commands.executeCommand("vscode.diff", e.uri, serverDoc.uri);
					var qpi: vscode.MessageItem[] = ["Overwrite", "Discard"].map((item) => { return { title: item, label: item } as vscode.MessageItem; });
					var answer = await vscode.window.showErrorMessage("Concurrent update!", { modal: true } as vscode.MessageOptions, ...qpi);
					if (answer?.title == "Overwrite") {
						infoMessage("Overwrote the file!", e.uri.path);
					}
					else {
						errorMessage("Concurrent update!", e.uri.path);
					}
				}
			}
			if (!data.path) {
				throw "No path set for uploading to workspace!";
			}
			let $this = this;
			let payload: KurmiWorkspaceEditor.FileResult = { workspace: cs.workspaceName, path: data.path, content: e.getText(), revisionAuthor: vscode.env.appName, revisionComment: "Updated from vscode extension" };
			let printablePayload: any = { ...payload };
			printablePayload.content = printablePayload.content.substring(0, 27) + "...";
			printablePayload.origin = cs.connection.baseuri?.authority;
			infoMessage('saveFile', printablePayload);
			var result: any = await $this.prism.api.execute('addWorkspaceFile', payload).then(async function (res: any) {
				infoMessage("SaveFile", `File saved in workspace.\r\n${$this.getKurmiFileSimulatedUri(payload.workspace, payload.path)}   `);
				var newServerVersion: KurmiWorkspaceEditor.FileResult = await $this.prism.workspace.getWorkspaceFile(payload.workspace, payload.path);
				var data: Partial<KurmiWorkspaceEditor.FileResult> = { ...newServerVersion };
				if (data.content) {
					delete data.content;
				}
				if (cs) {
					newServerVersion.originHostName = cs && cs.connection.baseuri?.authority;
					newServerVersion.workspace = cs && cs.workspaceName;
				}
				else {
					throw "NO CS... COMMIT"
				}
				cs?.managedFileData.set(e.uri, data);
			}, function (e: any) {
				errorMessage('Save file', e);
				throw e;
			});
			return result;
		}
		async findConnection(u: vscode.Uri) {
			let c: Connection = this.sessionLocalFSMapping[u.toString()];
			if (c) {
				return new Connection(c.host, c.login, c.password, c.doSave, c.knickName);
			}
			else {
				let splits = (u.path).split(/[\\\/]+/);
				let ksld: any[] = JSON.parse(await this.context.secrets.get(kurmiappconstants.keyKurmiStoredLoginDatas) || "[]");
				let cc = ksld.find((m: any) => { return m && m.host && m.hostName && splits.some((s: string) => new RegExp(m.hostName, "i").test(s)) });
				if (cc) {
					return new Connection(cc.host, cc.login, cc.password, cc.doSave, cc.knickName);
				}
			}
		}
		async saveFSConnectionMapping() {
			this.setBaseURI();
			let c = this.currentSession?.connection;
			if (c && c.baseuri) {
				this.sessionLocalFSMapping[c?.baseuri?.toString()] = c;
				await this.context.workspaceState.update(kurmiappconstants.keyKurmiSessionLocalFSMapping, this.sessionLocalFSMapping);
			}
		}
		async initializeSession(sd: Partial<KurmiSessionState>) {
			sd.managedFileData = new FileStateDataBroker(this.context);
			sd.pathSeparator = sd.virtual ? "/" : path.sep;
			this.currentSession = sd as KurmiSessionState;
			//proxy function for prism.api.connect();
			if (this.currentSession.connection.connected !== true && typeof this.currentSession.connection.connect == "function") {
				await this.currentSession.connection.connect(prism);
				this.saveFSConnectionMapping();
				this.saveConnections();
				let currentFolder: vscode.WorkspaceFolder | undefined = vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0];
				if (this.currentSession && this.currentSession.connection.baseuri) {
					if (!currentFolder || currentFolder.uri != this.currentSession.connection.baseuri) {
						await vscode.workspace.fs.createDirectory(this.currentSession.connection?.baseuri);
						let lenFolders = vscode.workspace.workspaceFolders?.length || 0;
						//vscode.workspace.updateWorkspaceFolders(lenFolders,0,{uri:this.currentSession.connection?.baseuri, name: sd.connection?.host.authority});
						if (lenFolders > 0) {
							//vscode.workspace.updateWorkspaceFolders(0,lenFolders);
						}
					}
				}
			}
		}
		private setBaseURI() {
			let c = this.currentSession?.connection;
			if (c && this.currentSession?.virtual) {
				let base = vscode.Uri.parse(`${kurmiappconstants.virtualFileSystemSchemeName}://${c.host.authority}/`);
				c.baseuri = base
			}
			else if (c) {
				let keyTargetLocalWorkspaceRootDirectory: string = vscode.workspace.getConfiguration(kurmiappconstants.appname).get(kurmiappconstants.keyTargetLocalWorkspaceRootDirectory) || '';
				let base = vscode.Uri.file(keyTargetLocalWorkspaceRootDirectory);
				c.baseuri = vscode.Uri.joinPath(base, ...[c.host.authority]);
			}
			return c?.baseuri;
		}
		private saveConnections() {
			let ksld: any = this.context.secrets.get(kurmiappconstants.keyKurmiStoredLoginDatas) || {};
			this.connections.filter(c => c.doSave).forEach((c) => {
				ksld[c.host.toString()] = c;
			});
		}
		getFoldersList(wsFilesFiltered: string[]) {
			var $this = this;
			if (this.emulationType == KurmiFileSystemEmulation.flat) {
				return [];
			}
			if (this.emulationType == KurmiFileSystemEmulation.likeDesignMenu) {
				var list: string[] = [];
				wsFilesFiltered.map((w: string) => this.getKurmiDesignMenuStructureFromFilePath(w)).forEach(function (item: string) {
					item.split($this.currentSession?.pathSeparator || path.sep).forEach(function (splt, i, arr) {
						var propName = arr.slice(0, i + 1).join($this.currentSession?.pathSeparator || path.sep);
						list.push(propName);
					});
				});
				return list.filter((item: string, i: number, a: string[]) => a.indexOf(item) == i);
			}
			var $this = this;
			var folders: any = {};
			wsFilesFiltered.forEach(function (filePath: string) {
				filePath.split(forwardslash).slice(0, -1).forEach(function (splt, i, arr) {
					var propName = arr.slice(1, i + 1).join($this.currentSession?.pathSeparator || path.sep);
					folders[propName] = true;
				});
			});
			//build a list of directories by splitting the kurmi file paths and joining them at each iteration
			var folderList = Object.keys(folders).map(f => f.replace(/^[\\\/]+/, "").replace(/[\\\/]+$/, "")).filter(f => f != forwardslash && f != "").sort();
			return folderList;
		}
		getFileExtensionFromKurmiFilePath(filePath: string) {
			let [fileExtension] = filePath.match(/[\w\-\_]+\.\w+$/g) || [''];
			return fileExtension;
		}
		getKurmiDesignMenuStructureFromFilePath(filePath: string): string {
			//get the complementing design menu folder name from the kurmi file path
			var ps = this.currentSession?.pathSeparator;
			let fileExtension = this.getFileExtensionFromKurmiFilePath(filePath);
			if (fileExtension.length == 0) { return ""; }
			switch (fileExtension.toLowerCase()) {
				case "eventlogger.js": {
					return "Event logger"
				}
				case "apiutil.js": { }
				case "util.js": {
					return "Javascript libraries";
				}
				case "serviceorder.json": { }
				case "service.xml": {
					return `Services${ps}Service Factory`;
				}
				case "inference.js": {
					return `Services${ps}Inference`;
				}
				case "quickfeature.properties": { }
				case "quickfeature.js": { }
				case "quickfeature.xml": {
					return "Quick features";
				}
				case "advancedconnector.xml": { }
				case "connector.properties": { }
				case "connector.xml": { return "Connectors definitions" }
				case "core-entities.xml": { return "User definitions" }
				case "directorymodel.xml": { return "Directory connectors" }
				case "mail.js": { return "Emails" }
				case "rules.json": { }
				case "rulesset.json": { }
				case "ruleset.json": { }
				case "rule.json": { return "Engineering Rules" }
				case "import.js": { }
				case "export.js": { }
				case "componentadjustment.js": { }
				case "discovery.js": { }
				case "dashboardscenario.js": { }
				case "dashboardtenant.js": { }
				case "postprocessing.js": { }
				case "externaltask.js": { }
				case "kurmiapi.js": { }
				case "configuration.js": { }
				case "processing.js": { }
				case "data.json": { }
				case "choice.json": { }
				case "scenario.json": { }
				case "mergeadjust.js": { }
				case "schema.json": { }
				case "options.json": { return "Scenarios and schema" }
				case "widget.js": { return "Homepage widgets" }
				default: {
					warningMessage("Unsupported file extension: ", fileExtension);
					return "";
				}
			}
		}
		getKurmiDesignMenuStructurePathFromFilePath(filePath: string): string {
			//returns the design menu structure with the formatted real kurmi path as it would appear in the design menu items page;
			var fileExtension = this.getFileExtensionFromKurmiFilePath(filePath);
			var designPath = this.getKurmiDesignMenuStructureFromFilePath(filePath);
			var suffix = (fileExtension ? filePath.replace(/\//g, "_") : "");
			return [designPath].concat(suffix).join(this.currentSession?.pathSeparator);
		}
	}
	export interface ConnectionData {
		host: vscode.Uri;
		login: string;
		password: string;
	}
	export class Connection implements ConnectionData {
		prism: any;
		host: vscode.Uri;
		login: string;
		password: string;
		connected: boolean = false;
		lastConnection?: Date;
		doSave: boolean = false;
		knickName?: string;
		baseuri?: vscode.Uri;
		constructor(host: vscode.Uri, login: string, password: string, doSave: boolean, knickName?: string) {
			this.host = host;
			this.login = login;
			this.password = password;
			this.doSave = doSave;
			this.knickName = knickName;
		}
		async connect(prism: any) {
			this.prism = prism;
			this.prism.api.login(this.login, this.password, `${this.host.scheme}://${this.host.authority}`);
			this.connected = this.prism.api.isConnected;
			let c = await this.prism.api.connect();
			this.lastConnection = new Date();
			return c;
		}
	}
	export interface FileResult {
		path: string;
		workspace: string;
		revisionAuthor: string;
		revisionDate?: number;
		revisionComment: string;
		description?: string;
		content?: string;
		originHostName?: string
	}
	export interface KurmiSessionState {
		connection: Connection;
		workspaceName: string;
		managedFileData: any;
		virtual: boolean;
		pathSeparator?: string;
		fileQueue: string[];
	}
	export enum KurmiFileSystemEmulation {
		likeConfigFile = "Like configfile.zip", likeDesignMenu = "Like design menu", flat = "flat structure"
	}
	class FileStateDataBroker {
		workspaceState: any;
		constructor(context: vscode.ExtensionContext) {
			this.workspaceState = context.workspaceState;
		}
		get(uri: vscode.Uri) {
			if (!uri) return;
			var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
			var data = (this.workspaceState.get(kurmiappconstants.keyFileStateData) || {});
			return data[key];
		}
		set(uri: vscode.Uri, newFileData: any) {
			if (!uri) return;
			var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
			var data = (this.workspaceState.get(kurmiappconstants.keyFileStateData) || {});
			data[key] = newFileData;
			this.workspaceState.update(kurmiappconstants.keyFileStateData, data);
		}
		Hostget(hostName: string, workspace: string, uri: vscode.Uri) {
			var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
			var data = (this.workspaceState.get(kurmiappconstants.keyFileStateData) || {});
			var hostData = data && data[hostName];
			var hostWorkspaceData = hostData && hostData[workspace];
			var hostWorkspaceKey = hostWorkspaceData && hostWorkspaceData[key];
			return hostWorkspaceKey || {};
		}
		Hostset(hostName: string, workspace: string, uri: vscode.Uri, newFileData: any) {
			var key = uri.path.split(/[\\\/]+/).filter(s => s).join('/').toLowerCase();
			var data = (this.workspaceState.get(kurmiappconstants.keyFileStateData) || {});
			data[hostName] = data[hostName] || {};
			data[hostName][workspace] = data[hostName][workspace] || {};
			data[hostName][workspace][key] = data[hostName][workspace][key] || {};
			data[hostName][workspace][key] = newFileData;
			this.workspaceState.update(kurmiappconstants.keyFileStateData, data);
		}
	}
}