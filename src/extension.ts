// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'
import * as vscode from 'vscode';
import { KurmiWorkspaceEditor } from './kurmiVirtualWorkspace';
import { errorMessage, infoMessage } from './logger';
import { kurmiappconstants } from './KurmiBridgeConstants';
import { KurmiLoginState, getStoredLoginDatas, kurmiLogin, kurmiWorkspaceBroker } from './kurmiLoginBroker';
import { KurmiWorkspaceState, whenClauseContext } from './kurmiWorkspaceHelpers';
import * as path from 'path';
import { MemFS } from './fileSystemProvider';
// Get the path separator
const $prism = require('kurmi-prism/prism_onnode.js');
const deactivationCallbacks: Function[] = []
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	async function parseURIforKurmiPFWSFP(e: vscode.Uri) {
		if (!e) return;
		let targetLocalWorkspaceRootDirectory: string = vscode.workspace.getConfiguration(kurmiappconstants.appname).get(kurmiappconstants.keyTargetLocalWorkspaceRootDirectory) || '';
		var targetLocalWorkspaceUri = vscode.Uri.parse(targetLocalWorkspaceRootDirectory);
		let inxOfTargetPath = e.path.replace(/[\\\/]/g, path.sep).indexOf(targetLocalWorkspaceUri.path.replace(/[\\\/]/g, path.sep));
		if (inxOfTargetPath !== -1) {
			var segments = e.path.slice(inxOfTargetPath).replace(/[\\\/]/g, path.sep).replace(targetLocalWorkspaceUri.path.replace(/[\\\/]/g, path.sep), "").split(/[\\\/]/);
			var interpretedLoginData: any, path_workspace: string = '', filePath: string = '';
			let loginDatas = await getStoredLoginDatas(context);
			segments.forEach(function (segment: string, index: number, array: any[]) {
				if (segment) {
					var _interpretedLoginData = loginDatas.find(ld => new RegExp(segment, "i").test(ld.hostName + ld.knickname));
					if (_interpretedLoginData) {
						interpretedLoginData = _interpretedLoginData;
						path_workspace = array[index + 1];
						filePath = "/" + array.slice(index + 2).join("/");
					}
				}
			});
			return { loginData: interpretedLoginData, workspaceName: path_workspace, filePath: filePath };
		}
		return null;
	}
	deactivationCallbacks.push(async function () {
		vscode.workspace.updateWorkspaceFolders(0, vscode.workspace.workspaceFolders?.length);
		context.workspaceState.get(kurmiappconstants.keyKurmiLoginData);
		await context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, undefined);
		console.log(`${kurmiappconstants.keyKurmiLoginData} is now ${context.workspaceState.get(kurmiappconstants.keyKurmiLoginData)}`);
		vscode.workspace.updateWorkspaceFolders(0, vscode.workspace.workspaceFolders?.length);
	});
	let initialized = false;
	let fsinitialized = false;
	//let virtual = context.workspaceState.get(kurmiappconstants.keyFileSystemSchemeMode) === "virtual";
	let eventsubscriptions: any = [];
	let virtual = whenClauseContext.getFSMode(context);
	if (virtual) {
		context.subscriptions.push(vscode.workspace.registerFileSystemProvider(kurmiappconstants.virtualFileSystemSchemeName, new MemFS(), { isCaseSensitive: true }));
	}
	const prism = $prism;
	const kwemanager = new KurmiWorkspaceEditor.VSCodeExtensionDataManager(context, prism);
	async function setFolder() {
		let targeturi = kwemanager.currentSession?.connection.baseuri || vscode.Uri.parse('');
		if (targeturi) {
			let folderLength = vscode.workspace.workspaceFolders?.length || 0;
			vscode.workspace.updateWorkspaceFolders(0, folderLength, { uri: targeturi, name: kwemanager.currentSession?.connection.baseuri?.authority })
		}
	}
	async function initialize() {
		//await context.workspaceState.update(kurmiappconstants.keyKurmiLoginData,undefined);
		let progress: any, token: vscode.CancellationToken;
		const onInitialized: Promise<KurmiWorkspaceEditor.KurmiSessionState | undefined> = new Promise(async (resolve, rejects) => {
			if (vscode.workspace.workspaceFolders?.some(wf => wf.uri.scheme)) {
				//whenClauseContext.setFSReady(true, 'activate');
				initialized = true;
				fsinitialized = true;
				try {
					if (!kwemanager.currentSession) {
						let c = await kwemanager.findConnection(vscode.workspace.workspaceFolders[0].uri);
						if (c) {
							let ss: Partial<KurmiWorkspaceEditor.KurmiSessionState> = {};
							//ss.virtual = (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0].uri.scheme === kurmiappconstants.virtualFileSystemSchemeName) ? true : false;
							ss.virtual = await whenClauseContext.getFSMode(context) === "virtual";
							//ss.workspaceName = workspaceData.name;
							ss.connection = c;
							await kwemanager.initializeSession(ss);
							await ss.connection.connect(prism);
						}
					}
					resolve(kwemanager.currentSession);
				}
				catch (e) {
					rejects(e);
				}
			}
			resolve(kwemanager.currentSession);
		}).then(async (currentSession) => {
			if (!currentSession) {
				//await vscode.window.withProgress({ title: kurmiappconstants.appDisplayName, cancellable: true, location: vscode.ProgressLocation.Notification },
				//async (progress, token) => {
				try {
					if (!kwemanager.currentSession) {
						var passibleData: Partial<KurmiWorkspaceEditor.VSCodeWorkingData> = { progress, token };
						passibleData.mode = KurmiWorkspaceEditor.KurmiEditorMode.silent;
						passibleData.context = context;
						var loginData: KurmiLoginState = await kurmiLogin(passibleData as KurmiWorkspaceEditor.VSCodeWorkingData);
						let ss: Partial<KurmiWorkspaceEditor.KurmiSessionState> = {};
						//ss.virtual = (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0].uri.scheme === kurmiappconstants.virtualFileSystemSchemeName) ? true : false;
						//ss.virtual = context.workspaceState.get(kurmiappconstants.keyFileSystemSchemeMode) === "virtual";
						ss.virtual = whenClauseContext.getFSMode(context) === "virtual";
						//ss.workspaceName = workspaceData.name;
						ss.connection = new KurmiWorkspaceEditor.Connection(vscode.Uri.parse(loginData.hostName), loginData.login, loginData.password, loginData.saveData, loginData.knickname);
						await kwemanager.initializeSession(ss);
					}
				}
				catch (e) {
					errorMessage('Connect', e);
				}
				//});
			}
			return kwemanager.currentSession;
		});
		onInitialized.finally(() => {
			whenClauseContext.setReady(true, 'connect');
			deactivationCallbacks.push(function () { context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, undefined); })
		});
		console.log('preferred progress', kurmiappconstants.preferredProgressLocation);
		return await vscode.window.withProgress({ location: kurmiappconstants.preferredProgressLocation, title: kurmiappconstants.appDisplayName, cancellable: true },
			async ($progress, $token) => {
				progress = $progress;
				token = $token;
				if (token.isCancellationRequested) {
					throw "Cancelled!"
				}
				token.onCancellationRequested((e) => {
					throw "Cancelled!"
				})
				var ready = await onInitialized;
				return ready;
			});
	}
	function doNextTick() {
		let nextTick: any = context.workspaceState.get(kurmiappconstants.keyNextTick) || null;
		if (nextTick) {
			context.workspaceState.update(kurmiappconstants.keyNextTick, undefined).then(async function () {
				await vscode.window.withProgress({ location: vscode.ProgressLocation.Window, title: `Next Tick: ${nextTick.command}`, cancellable: true },
					async (progress, token) => {
						var counter = 3;
						let increment = 100 / counter;
						let waitInMS = 0;
						var doNextTickInterval = setInterval(async function () {
							counter--;
							if (token.isCancellationRequested) {
								clearInterval(doNextTickInterval);
								progress.report({ message: `Cancelled "${nextTick.command}"`, increment: 100 });
							}
							if (counter < 0) {
								clearInterval(doNextTickInterval);
								progress.report({ message: `Timeout "${nextTick.command}"`, increment: 100 });
							}
							var folder = vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0];
							if (folder && vscode.workspace.fs.isWritableFileSystem(folder.uri.scheme)) {
								clearInterval(doNextTickInterval);
								progress.report({ message: `Executing "${nextTick.command}"`, increment: 100 })
								return await vscode.commands.executeCommand(nextTick.command, nextTick.params);
							}
							else if (!folder) {
								progress.report({ message: `No folder found. Waiting ${waitInMS}`, increment: increment });
								//errorMessage("No Folder found", "NOT WRITABLE");
							}
							else {
								progress.report({ message: `${folder.uri.scheme} not writable. Waiting ${waitInMS}`, increment: increment });
								//errorMessage(folder.uri.scheme, "NOT WRITABLE");
							}
							waitInMS = 3000;
						}, waitInMS);
					});
			})
		}
	}
	/*
	export enum KurmiFileSystemEmulation {
		likeConfigFile = "Like configfile.zip", likeDesignMenu = "Like design menu", flat = "flat structure"
	}*/
	const KurmiFileSystemEmulationDescriptions: any = {
		likeConfigFile: "Emulates the file structure found in a workspace zip download", likeDesignMenu: "Emulates the structure of the workspaces design menu.", flat: "All files in one directory."
	}
	//async function initKurmi(isVirtual:boolean = true,emulationType:KurmiFileSystemEmulation = KurmiFileSystemEmulation.likeConfigFile) {
	vscode.workspace.onDidOpenTextDocument(e => {
		let { path, origin, workspace } = kwemanager.currentSession?.managedFileData.get(e.uri);
		vscode.window.setStatusBarMessage(`Opened: ${kwemanager.getKurmiFileSimulatedUri(workspace, path)}`);
	})
	vscode.workspace.onWillSaveTextDocument(td => {
		kwemanager.commitFile(td.document);
	})
	vscode.workspace.onWillCreateFiles(fce => {
		//fce.files
	})
	vscode.workspace.onWillRenameFiles(fwre => {
		fwre.files.forEach(({ oldUri, newUri }) => {
			kwemanager.renameFile(oldUri, newUri);
		})
	})
	let listFiles = vscode.commands.registerCommand('kurmi-ws-vscode-extension.list', function (e: vscode.Uri) {
		initialize().then(async ($currentSession: KurmiWorkspaceEditor.KurmiSessionState | undefined) => {
			if ($currentSession && $currentSession.connection.connected) {
				let mode = KurmiWorkspaceEditor.KurmiEditorMode.silent;
				vscode.window.withProgress({ title: "Clone Kurmi workspace", location: kurmiappconstants.preferredProgressLocation, cancellable: true }, async (progress, token) => {
					if (!$currentSession.workspaceName) {
						var workspace = await vscode.window.showQuickPick(await prism.workspace.getList(), { title: "Choose a workspace", canPickMany: false }, token);
						if (workspace) {
							$currentSession.workspaceName = workspace;
						}
					}
					if ($currentSession.workspaceName) {
						var files = await vscode.window.showQuickPick(kwemanager.fetch($currentSession.workspaceName), { canPickMany: true }, token);
						files?.forEach(async (fp, i, arr) => {
							await kwemanager.pullFile($currentSession.workspaceName, fp);
							progress.report({ message: fp, increment: 1 / arr.length })
						})
					}
				});
			}
		})
	});
	context.subscriptions.push(listFiles);
	let savefile = vscode.commands.registerCommand('kurmi-ws-vscode-extension.savefile', function (e: vscode.Uri) {
		initialize().then(async ($currentSession: KurmiWorkspaceEditor.KurmiSessionState | undefined) => {
			if ($currentSession && $currentSession.connection.connected) {
				var td = await vscode.workspace.openTextDocument(e);
				kwemanager.commitFile(td);
			}
		});
	});
	context.subscriptions.push(savefile);
	var uriTargetLocalPath: vscode.Uri;
	let initlocal = vscode.commands.registerCommand('kurmi-ws-vscode-extension.initlocal', async function (e: vscode.Uri) {
		whenClauseContext.setFSReady(true, 'initlocal');
		await context.workspaceState.update(kurmiappconstants.keyNextTick, undefined);
		await context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, undefined);
		whenClauseContext.setFSMode(context, 'local');
		var uriToTest = e || (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0].uri);
		var parsedPlatformWorkspaceFilePath = await parseURIforKurmiPFWSFP(uriToTest);
		if (parsedPlatformWorkspaceFilePath) {
			context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, parsedPlatformWorkspaceFilePath.loginData);
			context.workspaceState.update(kurmiappconstants.keyKurmiWorkspaceState, { name: parsedPlatformWorkspaceFilePath.workspaceName } as KurmiWorkspaceState);
		}
		else {
			console.warn('wasnt in it');
		}
		//let base = vscode.Uri.file(keyTargetLocalWorkspaceRootDirectory);
		//let lenFolders = vscode.workspace.workspaceFolders?.length||0;
		//vscode.workspace.updateWorkspaceFolders(lenFolders,0, { uri: base, name: "Kurmi Local Workspaces" });
		//if (lenFolders > 0) {
		//	vscode.workspace.updateWorkspaceFolders(0,lenFolders);
		//}
		await initialize().then(function (currentSession) {
			if (parsedPlatformWorkspaceFilePath?.filePath) {
				context.workspaceState.update(kurmiappconstants.keyNextTick, { command: 'kurmi-ws-vscode-extension.pull', params: e });
			}
			else {
				context.workspaceState.update(kurmiappconstants.keyNextTick, { command: 'kurmi-ws-vscode-extension.pull', params: e });
			}
			setFolder();
			//if update folder does not cause the workspace to reload, then we try next tick
			doNextTick();
		})
	});
	context.subscriptions.push(initlocal);
	let cloneKurmi = vscode.commands.registerCommand('kurmi-ws-vscode-extension.clone', async function (e: any) {
		initialize().then(($currentSession: KurmiWorkspaceEditor.KurmiSessionState | undefined) => {
			if ($currentSession && $currentSession.connection.connected) {
				let mode = KurmiWorkspaceEditor.KurmiEditorMode.clone;
				vscode.window.withProgress({ title: "Clone Kurmi workspace", location: kurmiappconstants.preferredProgressLocation, cancellable: true }, async (progress, token) => {
					var workspaceData = await new kurmiWorkspaceBroker({ context, progress, token, mode }).initialize({ context, progress, token, mode });
					var workspaceTxtUri = $currentSession.connection.baseuri && vscode.Uri.joinPath($currentSession.connection.baseuri, ...[workspaceData.name, 'workspace.txt']);
					console.log('workspaceTxtUri', workspaceTxtUri);
					workspaceTxtUri && vscode.workspace.fs.writeFile(workspaceTxtUri, Buffer.from(`#vscode virtual workspace\r\n${workspaceData.name}\r\n#vscode virtual workspace\r\n`));
					workspaceTxtUri && vscode.commands.executeCommand('vscode.open', workspaceTxtUri);
					if ($currentSession) {
						$currentSession.fileQueue = workspaceData.specificFiles || [];
						kwemanager.pull(workspaceData.name);
						$currentSession.workspaceName = workspaceData.name;
					}
				});
			}
			else {
				errorMessage('pull', 'not connected');
			}
			console.log(e);
		})
	})
	context.subscriptions.push(cloneKurmi);
	let pullKurmi = vscode.commands.registerCommand('kurmi-ws-vscode-extension.pull', async function (e: vscode.Uri) {
		initialize().then(async ($currentSession: KurmiWorkspaceEditor.KurmiSessionState | undefined) => {
			if ($currentSession && $currentSession.connection.connected) {
				let mode = KurmiWorkspaceEditor.KurmiEditorMode.pull;
				let isNonKurmiFile = e && (e.scheme === "walkThrough" || vscode.workspace.getWorkspaceFolder(e));
				let fd = $currentSession.managedFileData.get(e);
				await vscode.window.withProgress({ title: "Pull from Kurmi workspace", location: kurmiappconstants.preferredProgressLocation, cancellable: true }, async (progress, token) => {
					var origin: string = '', kurmiFilePath: string = '', guessedWorkspace: string = '';
					var pulledFileSucessfully = false;
					if (!fd && e) {
						var ppwfp = await parseURIforKurmiPFWSFP(e);
						/*
						let workspaces = await prism.workspace.getList();
						var splits = e.path.split(/[\\\/]+/);
						guessedWorkspace = workspaces.find((w: string) => splits.some((s: string) => new RegExp(w, "i").test(s)));
						[origin] = splits.slice(splits.indexOf(guessedWorkspace) - 2);
						kurmiFilePath = "/" + splits.slice(splits.indexOf(guessedWorkspace)).join("/");
						*/
						guessedWorkspace = ppwfp?.workspaceName || '';
						origin = vscode.Uri.parse(ppwfp?.loginData.hostName).authority;
						kurmiFilePath = ppwfp?.filePath || '';
					}
					if (origin && guessedWorkspace && kurmiFilePath) {
						if (origin && origin == $currentSession.connection.host.authority) {
							kwemanager.pullFile(guessedWorkspace, kurmiFilePath);
							pulledFileSucessfully = true;
						}
						else if (origin && $currentSession.connection.baseuri?.path.indexOf(origin) !== -1) {
							kwemanager.pullFile(guessedWorkspace, kurmiFilePath);
							pulledFileSucessfully = true;
						}
					}
					else if (fd && $currentSession) {
						let { path, host, workspace, origin } = fd
						if (origin && origin == $currentSession.connection.baseuri?.authority) {
							kurmiFilePath = path;
							guessedWorkspace = workspace;
							kwemanager.pullFile(workspace, path);
							pulledFileSucessfully = true;
						}
					}
					else if ($currentSession) {
						var workspaceData = await new kurmiWorkspaceBroker({ context, progress, token, mode }).initialize({ context, progress, token, mode });
						//var filePaths = kwemanager.fetch(workspaceData.name);
						$currentSession.fileQueue = workspaceData.specificFiles || [];
						var workspaceTxtUri = $currentSession.connection.baseuri && vscode.Uri.joinPath($currentSession.connection.baseuri, ...[workspaceData.name, 'workspace.txt']);
						console.log('workspaceTxtUri', workspaceTxtUri);
						workspaceTxtUri && vscode.workspace.fs.writeFile(workspaceTxtUri, Buffer.from(`#vscode virtual workspace\r\n${workspaceData.name}\r\n#vscode virtual workspace\r\n`));
						workspaceTxtUri && vscode.commands.executeCommand('vscode.open', workspaceTxtUri);
						await kwemanager.pull(workspaceData.name);
						$currentSession.workspaceName = workspaceData.name;
					}
					if (isNonKurmiFile) {
						console.debug('nothing to do for pull, non kurmi file uri');
					}
					else if (pulledFileSucessfully) {
						vscode.window.setStatusBarMessage(`Merged file successfully: ${kwemanager.getKurmiFileSimulatedUri(guessedWorkspace, kurmiFilePath)}`);
					}
					else if (e) {
						errorMessage(`Failed to pull existing file "${e.path}."`)
					}
					else {
						console.error('Failed to pull a file that wasnt specified, check uri routing');
					}
				});
			}
			else {
				errorMessage('pull', 'not connected');
			}
			console.log(e);
		})
	})
	context.subscriptions.push(pullKurmi);
	let connectKurmi = vscode.commands.registerCommand('kurmi-ws-vscode-extension.connect', async function (e: any) {
		let openWorkspaceFolder = vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0];
		if (openWorkspaceFolder) {
			let pukpfwsfp = await parseURIforKurmiPFWSFP(openWorkspaceFolder?.uri);
			if (pukpfwsfp && pukpfwsfp.loginData) {
				vscode.commands.executeCommand('kurmi-ws-vscode-extension.initlocal', openWorkspaceFolder.uri)
			}
		}
		else {
			let [qinitvirtual, qinitlocal] = ["Connect Virtual FS", "Connect Local FS"];
			var items = [qinitvirtual, qinitlocal].map((title) => { return { title } as vscode.MessageItem; });
			if (true) {
				vscode.window.showInformationMessage("How do you want to load Kurmi Workspace?", { modal: false, detail: "Download files to your local file system or edit them live in memory." }, ...items).then((qPI) => {
					if (qPI) {
						if (qPI.title == qinitlocal) {
							vscode.commands.executeCommand('kurmi-ws-vscode-extension.initlocal', e)
						}
						else if (qPI.title == qinitvirtual) {
							vscode.commands.executeCommand('kurmi-ws-vscode-extension.initvirtual')
						}
					}
				}, (e: any) => {
					errorMessage('Connect', e);
				});
			}
			else {
				var qpitems = [qinitvirtual, qinitlocal].map((label) => { return { label } as vscode.QuickPickItem; });
				vscode.window.showQuickPick(qpitems, { title: "How do you want to load Kurmi Workspace?" }).then((qPI) => {
					if (qPI) {
						if (qPI.label == qinitlocal) {
							vscode.commands.executeCommand('kurmi-ws-vscode-extension.initlocal', e)
						}
						else if (qPI.label == qinitvirtual) {
							vscode.commands.executeCommand('kurmi-ws-vscode-extension.initvirtual')
						}
					}
				}, (e: any) => {
					errorMessage('Connect', e);
				});
			}
		}
	});
	context.subscriptions.push(connectKurmi);
	async function clearKeys(justClearThem: boolean = false) {
		context.workspaceState.keys().forEach((k) => {
			context.workspaceState.update(k, undefined);
			infoMessage(k, "cleared");
		})
	}
	let updateWorkspaceFolder = vscode.commands.registerCommand('kurmi-ws-vscode-extension.updateFolder', async (params) => {
		await setFolder();
	});
	context.subscriptions.push(updateWorkspaceFolder);
	let initvirtual = vscode.commands.registerCommand('kurmi-ws-vscode-extension.initvirtual', async (params) => {
		whenClauseContext.setFSReady(true, 'initvirtual');
		context.workspaceState.update(kurmiappconstants.keyNextTick, undefined);
		let fsmode = whenClauseContext.getFSMode(context);
		try {
			if (!vscode.workspace.fs.isWritableFileSystem(kurmiappconstants.virtualFileSystemSchemeName)) {
				context.subscriptions.push(vscode.workspace.registerFileSystemProvider(kurmiappconstants.virtualFileSystemSchemeName, new MemFS(), { isCaseSensitive: true }));
			}
			whenClauseContext.setFSMode(context, 'virtual');
		}
		catch (e: any) {
			errorMessage('initvirtual', e.message);
		}
		//vscode.workspace.updateWorkspaceFolders(0, vscode.workspace.workspaceFolders?.length || 0, { uri: vscode.Uri.parse('kurmifs://'), name: "Kurmi WE Virtual File System" });
		await context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, undefined);
		initialize().then(async function () {
			context.workspaceState.update(kurmiappconstants.keyNextTick, { command: 'kurmi-ws-vscode-extension.pull' });
			setFolder();
		})
	});
	context.subscriptions.push(initvirtual);
	let clear = vscode.commands.registerCommand('kurmi-ws-vscode-extension.clear', async () => {
		await clearKeys();
		vscode.window.showInformationMessage("Kurmi data cleared");
	});
	context.subscriptions.push(clear);
	let closeKurmi = vscode.commands.registerCommand('kurmi-ws-vscode-extension.close', async function () {
		eventsubscriptions.forEach((e: any) => {
			e.dispose();
		})
		clearKeys();
		whenClauseContext.setFSReady(false, "close");
		whenClauseContext.setReady(false, "close");
		var loginData = context.workspaceState.get(kurmiappconstants.keyKurmiLoginData);
		context.workspaceState.update(kurmiappconstants.keyKurmiLoginData, undefined);
		loginData = context.workspaceState.get(kurmiappconstants.keyKurmiLoginData);
		console.log("loginData was unset. Is now: ", loginData);
		var virtualFSFolderIndex = vscode.workspace.workspaceFolders?.findIndex((wf) => { return wf.uri.scheme == kurmiappconstants.virtualFileSystemSchemeName; });
		if (virtualFSFolderIndex != undefined && virtualFSFolderIndex != -1) {
			vscode.workspace.updateWorkspaceFolders(virtualFSFolderIndex, 1);
		}
		vscode.window.setStatusBarMessage("Kurmi is disconnected");
		deactivate();
	});
	context.subscriptions.push(closeKurmi);
	/*
	let refreshFiles = vscode.commands.registerCommand('kurmi-ws-vscode-extension.refresh', async function (e: any) {
		if (initialized == true) {
			vscode.window.withProgress({ cancellable: true, title: "Refresh workspace", location: vscode.ProgressLocation.Notification }, async (progress, token) => {
				let refreshSilentMode = true;
				await initKurmi(refreshSilentMode, progress, token).then(() => {
					ks.onReady.then(async () => {
						if (e) {
							let loginDatas = await getStoredLoginDatas(context);
							var splits = e.path.split(/[\\\/]+/);
							var interpretedLoginData: any, path_workspace: string = '', filePath: string = '';
							splits.forEach(function (segment: string, index: number, array: any[]) {
								if (segment && segment.trim()) {
									var _interpretedLoginData = loginDatas.find(ld => new RegExp(segment, "i").test(ld.hostName + ld.knickname));
									if (_interpretedLoginData) {
										interpretedLoginData = _interpretedLoginData;
										path_workspace = array[index + 1];
										filePath = array.slice(index + 2).join(ks.pathSeparator);
									}
								}
							});
							if (filePath) {
								ks.createVirtualFile(filePath);
							}
						}
						else {
							await ks.populateFiles(progress, token);
						}
					});
				});
			})
		}
		else {
			errorMessage('kurmi-ws-vscode-extension.refresh', "Not initialized");
		}
	});
	context.subscriptions.push(refreshFiles);*/
	doNextTick();
}
// this method is called when your extension is deactivated
export function deactivate() {
	deactivationCallbacks.forEach(cb => cb());
}
